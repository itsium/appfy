class AddIconUnicodeToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :icon_unicode, :string
  end
end
