class AddAttachmentIconSplashScreenToApps < ActiveRecord::Migration
  def self.up
    change_table :apps do |t|
      t.attachment :icon
      t.attachment :splash_screen
    end
  end

  def self.down
    drop_attached_file :apps, :icon
    drop_attached_file :apps, :splash_screen
  end
end
