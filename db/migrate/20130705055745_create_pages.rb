class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :json_conf
      t.integer :section_id
      t.integer :parent_page_id

      t.timestamps
    end
  end
end
