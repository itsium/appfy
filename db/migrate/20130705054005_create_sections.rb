class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string :name
      t.string :icon
      t.string :pagetype
      t.integer :position
      t.integer :app_id

      t.timestamps
    end
  end
end
