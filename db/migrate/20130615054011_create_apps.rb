class CreateApps < ActiveRecord::Migration
  def change
    create_table :apps do |t|
      t.string :name
      t.integer :category_id
      t.text :description
      t.integer :user_id

      t.timestamps
    end
    add_index :apps, :name
    add_index :apps, :user_id
  end
end
