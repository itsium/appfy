PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
INSERT INTO "categories" VALUES(1,'商业','icon-building','2013-06-15 05:51:32.046756','2013-06-15 07:10:35.102471','&#xf0f7;');
INSERT INTO "categories" VALUES(2,'新闻报刊','icon-book','2013-06-15 06:01:34.879973','2013-06-15 07:24:29.723634','&#xf02d;');
INSERT INTO "categories" VALUES(3,'财务','icon-money','2013-06-15 06:02:45.329951','2013-06-15 07:24:51.495719','&#xf0d6;');
INSERT INTO "categories" VALUES(4,'工具','icon-wrench','2013-06-15 06:12:21.269801','2013-06-15 07:25:05.227158','&#xf0ad;');
INSERT INTO "categories" VALUES(5,'健康医疗','icon-plus','2013-06-15 06:12:57.675778','2013-06-15 07:25:23.803006','&#xf067;');
INSERT INTO "categories" VALUES(6,'教育','icon-edit-sign','2013-06-15 06:14:58.345634','2013-06-15 07:25:36.082276','&#xf14b;');
INSERT INTO "categories" VALUES(7,'旅行','icon-plane','2013-06-15 06:15:47.028111','2013-06-15 07:25:48.920498','&#xf072;');
INSERT INTO "categories" VALUES(8,'美食','icon-food','2013-06-15 06:16:31.179319','2013-06-15 07:28:19.889996','&#xf0f5;');
INSERT INTO "categories" VALUES(9,'产品介绍','icon-star','2013-06-15 06:19:45.330777','2013-06-15 07:28:09.433340','&#xf005;');
INSERT INTO "categories" VALUES(10,'社交','icon-group','2013-06-15 06:20:38.573439','2013-06-15 07:27:54.655616','&#xf0c0;');
INSERT INTO "categories" VALUES(11,'摄影与图片','icon-camera','2013-06-15 06:21:25.711955','2013-06-15 07:27:41.662197','&#xf030;');
INSERT INTO "categories" VALUES(12,'生活','icon-home','2013-06-15 06:22:36.521392','2013-06-15 07:27:26.708357','&#xf015;');
INSERT INTO "categories" VALUES(13,'体育','icon-trophy','2013-06-15 06:24:18.079461','2013-06-15 07:27:11.379100','&#xf091;');
INSERT INTO "categories" VALUES(14,'效率','icon-laptop','2013-06-15 06:24:52.578773','2013-06-15 07:26:59.658475','&#xf109;');
INSERT INTO "categories" VALUES(15,'娱乐','icon-smile','2013-06-15 06:26:00.023449','2013-06-15 07:26:47.326164','&#xf118;');
COMMIT;
