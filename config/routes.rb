Appfy::Application.routes.draw do
  delete "section/destroy"

  match 'pages/:id/add_component' => 'pages#add_component',
        :as => :pages_add_component,  :via => :post
  match 'pages/:id/delete_component' => 'pages#delete_component',
        :as => :pages_delete_component,  :via => :post
  match 'pages/:id/update_component' => 'pages#update_component',
        :as => :pages_update_component, :via => :post
  match 'pages/:id/components' => 'pages#components',
        :as => :pages_components, :via => :get

  resources :apps do
    member do
    	get 'data'
    	get 'design'
    	get 'admin_panel'
      post 'add_section'
      post 'delete_section'
      get 'preview'
    end
  end


  resources :categories


  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"
  devise_for :users
  resources :users
end
