class Page < ActiveRecord::Base
  attr_accessible :json_conf, :parent_page_id, :section_id, :title
  attr_accessor :_pageconfig
  belongs_to :section
  has_many :child_pages, :class_name => "Page", :foreign_key => "parent_page_id"
  belongs_to :parent_page, :class_name => "Page"
  before_save  { |page| page.pageconfig.save_config if page.pageconfig }

  def pageconfig
    if self._pageconfig == nil
      self._pageconfig = PageConfig.new(self)
    end
    self._pageconfig
  end

end

class PageConfig
  attr_accessor :components, :page

  def pconf_serialize
    cmps = []
    @components.each do |c|
      if c == "text"
        cmps << {
          :xtype => "text",
          :data => "<h3>I am a <i>text</i></h3>"
        }
      elsif c == "image"
        cmps << {
          :xtype => "image",
          :data => "/image.jpg"
        }
      elsif c == "map"
        cmps << {
          :xtype => "map",
          :data => "/map.jpg"
        }
      elsif c == "mail"
        cmps << {
          :xtype => "mail",
          :data => "contact@test.com"
        }
      elsif c == "gallery"
        cmps << {
          :xtype => "gallery",
          :config => {
            :loop => true
          },
          :data => ["/gallery01.jpg", "/gallery02.jpg", "/gallery03.jpg"]
        }
      elsif c == "movie"
        cmps << {
          :xtype => "movie",
          :data => "/movie.mp4"
        }
      elsif c.class == String
        cmps << {
          :xtype => c
        }
      else
        cmps << c
      end
    end
    @page.json_conf = cmps.to_json
  end

  def pconf_unserialize
    @components = JSON.parse(@page.json_conf)
  end

  def save_config(save_parent = false)
    pconf_serialize
    @page.save if save_parent
  end

  def <<(obj)
    @components << obj
  end

  def initialize(page)
    @page = page
    (@page.json_conf) ? (pconf_unserialize) : (@components = [])
  end

  def to_s
    @components.to_s
  end
end
