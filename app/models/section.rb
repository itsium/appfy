class Section < ActiveRecord::Base
  attr_accessible :app_id, :icon, :name, :pagetype, :position
  belongs_to :app
  has_one :page, :dependent => :delete
  validates_presence_of :name, on: :create, message: "can't be blank"
  validates_presence_of :pagetype, on: :create, message: "can't be blank"
end
