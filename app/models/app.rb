class App < ActiveRecord::Base
  attr_accessible :category_id, :description, :name, :user_id, :splash_screen, :icon
  
  belongs_to :user
  belongs_to :category
  has_attached_file :icon, :styles => { :normal => "72x72", :retina => "114x114" }, :default_url => "/no_icon.png"
  has_attached_file :splash_screen, :styles => { :iphone5 => "1136x640", :iphone4 => "960x640" }, :default_url => "/no_splash_screen.png"
  has_many :sections, :dependent => :delete_all
end
