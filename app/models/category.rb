class Category < ActiveRecord::Base
  attr_accessible :icon_css, :name, :icon_unicode
  has_many :apps

  def select_label
  	"#{self.icon_unicode} #{self.name}".html_safe
  end
end
