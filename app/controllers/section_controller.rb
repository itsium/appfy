class SectionController < ApplicationController
  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @section = Section.find(params[:id])
    @success = false

    if @section.app.user.id == current_user.id
      @section.destroy
      @success = true
    else
    	@section.errors << "Errors while deleting section."
      @message = "Errors while deleting section."
    end

    #respond_to do |format|
      #format.html { redirect_to :back }
      #format.json { head :no_content }
    #end
  end
end
