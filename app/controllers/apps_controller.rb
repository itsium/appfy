class AppsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :current_app, except: [:new, :create ]

  def current_app
    @current_app = App.find(params[:id])
    if @current_app.user.id != current_user.id
      flash[:error] = "You are not the owner of this app !"
      @current_app = nil
      redirect_to current_user.apps.first
    end
  end

  # GET /apps
  # GET /apps.json
  def index
  end

  # GET /apps/1
  # GET /apps/1.json
  def show
    @app = @current_app

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @app }
    end
  end

  # GET /apps/new
  # GET /apps/new.json
  def new
    @app = App.new
    @app.user = current_user
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @app }
    end
  end

  # GET /apps/1/edit
  def edit
    @app = App.find(params[:id])
  end

  # POST /apps
  # POST /apps.json
  def create
    @app = App.new(params[:app])
    @app.user = current_user
    respond_to do |format|
      if @app.save
        format.html { redirect_to @app, notice: 'App was successfully created.' }
        format.json { render json: @app, status: :created, location: @app }
      else
        format.html { render action: "new" }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /apps/1
  # PUT /apps/1.json
  def update
    @app = @current_app
    @app.user = current_user
    respond_to do |format|
      if @app.update_attributes(params[:app])
        format.html { redirect_to @app, notice: 'App was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /apps/1
  # DELETE /apps/1.json
  def destroy
    @app = @current_app
    @app.destroy

    respond_to do |format|
      format.html { redirect_to apps_url }
      format.json { head :no_content }
    end
  end

  def design
    @app = @current_app
    @section = Section.new
  end

  def add_section
    @app = @current_app
    @section = @app.sections.new(params[:section])
    @section.page = Page.new({section_id: @section.id})
    if @section.save
      @success = true
      @message = t("new_section", default: "New section is created")
      redirect_to :back
    else
      @success = false
      @message = @section.errors
      flash[:error] = @section.errors
      redirect_to :back
    end
  end

  def delete_section
    @app = @current_app
    @section = @app.sections.find(params[:section_id])
    @section.delete if @section
    logger.debug "[DEBUG] #{@section.inspect} "
    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  def data

  end

  def admin_panel

  end

  def preview

    app = App.find(params[:id])

    sections = []
    pages = {}
    app.sections.each do |s|
      sections << {
        :id => s.id.to_s,
        :xtype => s.pagetype,
        :icon => s.icon,
        :name => s.name
      }
      pages[s.id] = {
        :id => s.page.id.to_s,
        :items => JSON.parse(s.page.json_conf)
      }
    end

    @appconfig = {
      :name => app.name,
      :sections => sections,
      :pages => pages
    }

    render layout: false
  end
end
