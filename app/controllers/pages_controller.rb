class PagesController < ApplicationController
  before_filter :init_parameter
  before_filter :get_current_page

  def add_component
  	if params[:component] && @existant_component.include?(params[:component])
 		@component = params[:component]
  		@page.pageconfig << @component
  		@page.save
  		@success = true
  		@message = "New Component is created."
      # <@REFACTORING>
      self.formats = [:html]
      @html = render_to_string(
        :file => "apps/_home_design_panel_item.html.erb",
        :layout => false,
        :locals => {
          c: @component,
          i: nil,
          sec: @page.section
      })
      self.formats = [:json]
      # </@REFACTORING>
  	else
  		@success = false
  		@message = "Component don't exist."
   	end
  end

  def components
  	@components = @page.pageconfig.components
  end

  def delete_component
    @array_id = params[:array_id]
    if @array_id
      if @page.pageconfig.components.delete_at(@array_id.to_i)
        @page.save
        @success = true
        @message = "New Component is deleted."
      else
        @success = false
        @message = "Component don't exist."
      end
    else
      @success = false
      @message = "Component don't exist."
    end
  end

  def update_component

  end

  def save_config
  	@page.pageconfig.save_config(true)
  end

private
  def init_parameter
  	@existant_component = %w(text image movie galery separator map mail)
  end

  def get_current_page
  	@app = App.find(params[:id])
  	@section = Section.find(params[:sec_id])
  	if @app == nil || @section == nil || @app.user.id != current_user.id || @section.app.id != @app.id
  		flash[:error] = "App id error."
  		redirect_to :back
  	end
  	@page = @section.page
  end
end
