var active = false;

$('#myTab').children().each(function() {
    if ($(this).hasClass('active') == true) {
        active = true;
    }
});

if (active == false) {
    $( '#myTab li' ).first().toggleClass('active');
    $( '#menu_design' ).toggleClass('active');
}

function onclickDeleteSection() {

    if (confirm('Are you sure ?') === true) {

        var id = $(this).attr('id-section'),
            url = urls.deleteSection + '.json?id=' + id,
            data = {
                _method: 'delete'
            };

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: data
        }).done(function(data) {

            if (data && data.success === true) {
                $('#tab-' + id).remove();
                $('#myTab li').first().addClass('active');
                $('#menu_design').addClass('active');
            }

        });

    } else {
        return false;
    }

};

function recalculateArrayID() {
    var i = -1;

    $('.tab-pane.active .accordion-group').each(function() {
        // la premiere boxe de titre n'a pas besoin de ca
        if (i !== -1) {
            $(this).find('a.delete_component').attr('data-array-id', i);
            $(this).find('a[data-toggle]').attr('href', '#collapse-' + i);
            $(this).find('.accordion-body').attr('id', 'collapse-' + i);
        }
        ++i;
    });
};

function addComponent(data) {
    if (data.success == true) {
        var a = $('#accordion-' + this.attr('data-id')),
            win = window;

        if (data.html) {
            a.append(data.html.replace(/\\n/g, '\n').replace(/\\"/g, '"'));
            win.recalculateArrayID();

            if (data.component === 'text') {
                win.createMarkdown($('textarea[data-provide="markdown"]').last());
            } else if (data.component === 'map') {
                win.initMap('map-container');
            } else if (data.component === 'image') {
                $('.image-component').last().ace_file_input({
                    no_file: 'No File ...',
                    btn_choose: 'Choose',
                    btn_change: 'Change',
                    droppable: false,
                    onchange: null,
                    thumbnail: true //| true | large
                    //whitelist:'gif|png|jpg|jpeg'
                    //blacklist:'exe|php'
                    //onchange:''
                    //
                });
            }

            $('.delete_component').last().on('click', win.onclickDeleteComponent);

        }
    }
};

function deleteComponent(data) {
    if (data.success == true) {
        var el = this;

        while (el.hasClass('accordion-group') !== true) {
            el = el.parent();
        }
        if (el.hasClass('accordion-group')) {
            el.remove();
        }

        // update delete buttons ids
        recalculateArrayID();
    }
};

function onclickAddComponent() {
    var self = $(this),
        url = urls.addComponent + '.json?component=' +
            $('.pagetypepicker').val() + '&sec_id=' +
            self.attr('data-id'),
        data = {
            _method: 'post'
        };

    $.post(url, data, addComponent.bind(self), 'json');
    $('#myModal').modal('hide');
};

function onclickDeleteComponent() {

    var self = $(this),
        url = urls.deleteComponent +
            '?array_id=' + self.attr('data-array-id') +
            '&sec_id=' + self.attr('data-sec-id'),
        data = {
            _method: 'post'
        };

    $.post(url, data, deleteComponent.bind(self), 'json');
};

function onclickSearchMap() {

    var secId = $(this).attr('data-sec-id'),
        keyword = $('#map-search-term-' + secId).val();

    baidu_map.clearOverlays();
    baidu_localSearch.setSearchCompleteCallback(function (searchResult) {
        var poi = searchResult.getPoi(0);

        baidu_map.centerAndZoom(poi.point, 16);

        var marker = new BMap.Marker(new BMap.Point(poi.point.lng, poi.point.lat));
        baidu_map.addOverlay(marker);

        var content = keyword + '<br/><br/>经度：' + poi.point.lng + '<br/>纬度：' + poi.point.lat;
        var infoWindow = new BMap.InfoWindow('<p style="font-size:14px;">' + content + '</p>');

        marker.addEventListener('click', function () {
            this.openInfoWindow(infoWindow);
        });
    });
    baidu_localSearch.search(keyword);
};

// listeners
$('.delete_section').on('click', onclickDeleteSection);
$('#add_component_bt').on('click', onclickAddComponent);
$('.delete_component').on('click', onclickDeleteComponent);
$('.search_map').on('click', onclickSearchMap);
