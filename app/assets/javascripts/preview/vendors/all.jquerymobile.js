

if (!Function.prototype.bind) {
    Function.prototype.bind = function(oThis) {
        if (typeof this !== 'function') {
            
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function() {},
            fBound = function() {
                return fToBind.apply(this instanceof fNOP ? this : oThis || window,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}


'use strict';

(function() {

    var VERSION = '1.6',
        slice = Array.prototype.slice,
        _xtypes = {};

    var _xtype = function(xtype, obj) {
        if (obj) {
            _xtypes[xtype] = obj;
        } else {
            return (_xtypes[xtype] ||
                console.warn('[core] xtype "' + xtype +
                    '" does not exists.'));
        }
    };

    var _createSubclass = function(props) {
        props = props || {};

        var key,
            realConstructor,
            superclass = props.superclass.prototype;

        if (props.hasOwnProperty('constructor')) {
            realConstructor = props.constructor;
        } else if (typeof superclass.constructor === 'function') {
            realConstructor = superclass.constructor;
        } else {
            realConstructor = function() {};
        }

        function constructor() {
            if (!(this instanceof constructor)) {
                throw new Error('Please use "new" when initializing Fs classes');
            }
            realConstructor.apply(this, arguments);
        }

        constructor.prototype = Object.create(superclass);
        constructor.prototype.constructor = constructor;

        _extend(constructor, {

            parent: superclass,

            subclass: function(obj) {
                var sclass;

                obj = obj || {};
                obj.superclass = this;
                sclass = _createSubclass(obj);

                if (obj.xtype) {
                    _xtype(obj.xtype, sclass);
                }
                return sclass;
            }
        });

        for (key in props) {
            if (key !== 'constructor' &&
                key !== 'superclass') {
                constructor.prototype[key] = props[key];
            }
        }

        return constructor;
    };

    var _extend = function() {
        var i = -1,
            args = slice.call(arguments),
            l = args.length,
            object = args.shift();

        while (++i < l) {
            var key,
                props = args[i];

            for (key in props) {
                object[key] = props[key];
            }
        }
        return object;
    };

    var Fs = {
        views: {},
        events: {},
        data: {},
        engines: {},
        config: {},
        helpers: {},
        xtype: _xtype,
        version: VERSION,

        subclass: function(obj) {
            var sclass;

            obj = obj || {};
            obj.superclass = function() {};
            sclass = _createSubclass(obj);

            if (obj.xtype) {
                _xtype(obj.xtype, sclass);
            }
            return sclass;
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' &&
            module.exports) {
            exports = module.exports = Fs;
        }
        exports.Fs = Fs;
    } else if (typeof define === 'function' &&
        define.amd) {
        define(function() {
            return Fs;
        });
    } else {
        window.Fs = Fs;
    }

})();


Fs.debug = new (function() {

    var _profiles = {},
        _debug = false,
        _whitelist = [];

    var isWhitelist = function(prefix) {
        if (_whitelist.length &&
            _whitelist.indexOf(prefix) === -1) {
            return false;
        }
        return true;
    };

    return Fs.subclass({

        profile: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.time(key);
            
            return this;
        },

        log: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.log.apply(console, args);
            return this;
        },

        error: function(prefix) {
            if (!_debug) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.error.apply(console, args);
            return this;
        },

        warn: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.warn.apply(console, args);
            return this;
        },

        profileEnd: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.timeEnd(key);
            
            
            
            return this;
        },

        memory: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            if (window.performance && window.performance.memory) {
                return ((window.performance.memory.totalJSHeapSize / 1024.0) + 'ko');
            }
            return 0;
        },

        set: function(debug) {
            _debug = (debug === true);
        }
    });

}())();


Fs.utils = (function() {

    var _table = '00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D';

    return {
        
        applyIf: function(obj, base, keys) {
            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof obj[key] === 'undefined' &&
                    typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        applyIfAuto: function(obj, base) {
            var key;

            for (key in base) {
                if (typeof obj[key] === 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        apply: function(obj, base, keys) {
            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        applyAuto: function(obj1, obj2) {
            var obj3 = {};

            for (var attrname in obj1) {
                obj3[attrname] = obj1[attrname];
            }
            for (var attrname in obj2) {
                obj3[attrname] = obj2[attrname];
            }
            return obj3;
        },

        arraySlice: function(array, from) {
            var result = [],
                i = from - 1,
                length = array.length;

            while (++i < length) {
                result.push(array[i]);
            }
            return result;
        },

        capitalize: function(str) {
            return (str.charAt(0).toUpperCase() + str.slice(1));
        },

        crc32: function(str, crc) {
            if (crc == window.undefined) {
                crc = 0;
            }

            var i = 0,
                iTop = str.length,
                n = 0, 
                x = 0; 

            crc = crc ^ (-1);
            for (; i < iTop; i++ ) {
                n = ( crc ^ str.charCodeAt(i)) & 0xFF;
                x = '0x' + _table.substr(n * 9, 8);
                crc = (crc >>> 8) ^ x;
            }
            return crc ^ (-1);
        }
    };

}());

Fs.Storage = new (function() {

    var _store = localStorage,
        _prefix = '';

    var _setPrefix = function(prefix) {
        if (typeof prefix === 'string') {
            _prefix = prefix;
            return true;
        }
        return false;
    };

    var _getPrefix = function(prefix) {
        if (typeof prefix === 'string') {
            return prefix;
        }
        return _prefix;
    };

    var _setItem = function(name, value, prefix) {
        return (_store.setItem(_getPrefix(prefix) + name,
            JSON.stringify(value)));
    };

    var _getItem = function(name, prefix) {
        return JSON.parse(_store.getItem(_getPrefix(prefix) + name));
    };

    var _removeItem = function(name, prefix) {
        return _store.removeItem(_getPrefix(prefix) + name);
    };

    var _empty = function(prefix) {
        prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (!prefix || !name.indexOf(prefix)) {
                _removeItem(name, '');
            }
        }
    };

    return Fs.subclass({

        setPrefix: _setPrefix,
        getPrefix: _getPrefix,
        setItem: _setItem,
        getItem: _getItem,
        removeItem: _removeItem,
        empty: _empty

    });

}())();

Fs.Selector = new (function() {
    
    var _scope,
        _elUid = 0,
        _win = window;

    
    return Fs.subclass({
        constructor: function(gscope) {
            _scope = gscope;
        },

        generateId: function(prefix) {
            ++_elUid;
            prefix = prefix || 'fs';
            return (prefix + _elUid);
        },

        get: function(selector, root) {
            root = root || _scope;
            return root.querySelector(selector);
        },

        gets: function(selector, root) {
            root = root || _scope;
            return root.querySelectorAll(selector);
        },

        hasClass: function(el, cls) {
            var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            
            if (!el || typeof el !== 'object') {
                console.warn('EL is not good', el, arguments.callee.caller.caller);
                return false;
            }
            
            return el.className.match(reg);
        },

        addClass: function(el, cls) {
            if (!this.hasClass(el, cls)) {
                
                el.className += ' ' + cls;
                el.className = el.className.replace(/^\s+|\s+$/g, '');
                return true;
            }
            return false;
        },

        removeClass: function(el, cls) {
            if (this.hasClass(el, cls)) {
                
                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                el.className = el.className.replace(reg, ' ').replace(/^\s+|\s+$/g, '');
                return true;
            }
            return false;
        },

        redraw: function(el) {
            el.style.display = 'none';
            el.offsetHeight;
            el.style.display = 'block';
        },

        removeEl: function(el) {
            if (typeof el === 'string') {
                el = this.get(el);
            }
            el.parentNode.removeChild(el);
        },

        removeHtml: function(el) {
            
            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }
        },

        addHtml: function(el, html) {
            el.insertAdjacentHTML('beforeEnd', html);
        },

        updateHtml: function(el, html) {
            this.removeHtml(el);
            this.addHtml(el, html);
        },

        scrollToEl: function(el, offsetX, offsetY) {
            offsetX = offsetX || 0;
            offsetY = offsetY || 0;
            _win.scrollTo(el.offsetLeft + offsetX,
                el.offsetTop + offsetY);
        }
    });
}())(document);


Fs.Request = new (function() {
    
    var jsonp_id = 0,
        _debug = Fs.debug;

    var _jsonToString = function(json, prefix) {
        var key,
            val,
            result = '',
            first = true;

        for (key in json) {
            val = json[key];
            if (first === false) {
                result += '&';
            } else {
                first = false;
            }
            result += key + '=' + encodeURIComponent(val);
        }
        _debug.log('request', 'jsonToString result', result);
        return (prefix ? prefix : '') + result;
    };

    
    return Fs.subclass({
        
        ajax: function(url, options) {
            var request,
            callback = (typeof options.callback === 'function' ?
                options.callback.bind(options.scope || window) : null);
            options.method = options.method || 'GET';
            options.data = options.data || '';

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            function stateChange() {
                if (request.readyState === 4 &&
                    callback) {
                    callback((request.status === 200 ||
                        request.status === 304) ? true : false,
                        request.responseText);
                }
            }

            request = new XMLHttpRequest();
            request.onreadystatechange = stateChange;
            request.open(options.method, url, true);
            if (options.method !== 'GET' && options.data) {
                request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                request.setRequestHeader('Content-type',
                    'application/x-www-form-urlencoded');
                request.setRequestHeader('Connection', 'close');
            }
            request.send(options.data);
        },

        
        jsonp: function(url, options) {
            var callback_name = ['jsonp', ++jsonp_id].join(''),
                callback = options.callback.bind(options.scope || window),
                script = document.createElement('script'),
                clean = function() {
                    document.body.removeChild(script);
                    delete window[callback_name];
                },
                error = function() {
                    _debug.warn('request', 'JSONP request error', url, options, arguments);
                    clean();
                    if (callback) {
                        callback(false);
                    }
                },
                success = function(data) {
                    clean();
                    if (callback) {
                        callback(true, data);
                    }
                };

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            window[callback_name] = success;
            script.onerror = error;
            script.src = url.replace('{callback}', callback_name);
            document.body.appendChild(script);
        },

        memory: function(data, options, store) {
            var i, length, callback,
                result = {},
                idProp = store.getReaderId(),
                totalProp = store.getReaderTotal(),
                successProp = store.getReaderSuccess(),
                rootProp = store.getReaderRoot();

            callback = options.callback.bind(options.scope || window);
            if (!data) {
                data = store.config.data || [];
            }
            length = data.length;
            if (data[0] && typeof data[0][idProp] === 'undefined') {
                for (i = 0; i < length; ) {
                    data[i][idProp] = ++i;
                }
            }
            result[totalProp] = length;
            result[successProp] = true;
            result[rootProp] = data;
            if (callback) {
                callback(true, result);
            }
        }
    });
}())();


Fs.History = new (function() {

    
    var _fs = Fs,
        _win = window,
        scope = _win,
        _storage = _fs.Storage,
        _isStandalone = (_win.navigator && _win.navigator.standalone === true),
        curhash = _win.location.hash.substr(1),
        routes = [];

    function runCallbacks(location) {
        
        if (_isStandalone) {
            _storage.setItem('location', location, _win.location.host);
        }
        this.curhash = location;
        var regexp, i = -1, length = routes.length, route;
        while (++i < length) {
            route = routes[i];
            if (route.regexp.test(location)) {
                route.callback(location);
            }
        }
        return true;
    };

    function onHashChange(event) {
        runCallbacks.call(this, _win.location.hash.substr(1));
    };

    
    return _fs.subclass({
        constructor: function(gscope) {
            this.defaultRoute = '';
            scope = gscope || _win;
            onHashChange = onHashChange.bind(this);
            scope.addEventListener('hashchange', onHashChange);
        },

        setDefaultRoute: function(path) {
            this.defaultRoute = path;
        },

        start: function() {
            var hash = curhash;
            curhash = '';

            
            if (_isStandalone) {
                hash = _storage.getItem('location', _win.location.host) || this.defaultRoute;
            }
            runCallbacks.call(this, hash);
        },

        stop: function() {
            scope.removeEventListener('hashchange', onHashChange);
        },

        here: function(encoded) {
            var hash = _win.location.hash.substr(1);

            return (encoded ? encodeURIComponent(hash) : hash);
        },

        navigate: function(location) {
            _win.location.hash = '#' + location;
        },

        route: function(route, callback) {
            routes.push({
                regexp: route,
                callback: callback
            });
        }
    });
}())(window);


Fs.Router = (function() {

    
    var optionalParam = /\((.*?)\)/g,
        namedParam = /(\(\?)?:\w+/g,
        splatParam = /\*\w+/g,
        escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g,

        extractParameters = function(route, fragment) {
            return route.exec(fragment).slice(1);
        },

        routeToRegExp = function(route) {
            route = route.replace(escapeRegExp, '\\$&')
                .replace(optionalParam, '(?:$1)?')
                .replace(namedParam, function(match, optional) {
                    return optional ? match : '([^\/]+)';
                })
                .replace(splatParam, '(.*?)');
            return new RegExp('^' + route + '$');
        },

        _prepareBefore = function() {
            if (this.before) {
                this.beforeRegexp = {};
                for (var i in this.before) {
                    
                    this.beforeRegexp[i] = new RegExp(i);
                }
            }
        },

        _getBefore = function(funcname) {
            if (this.before) {
                for (var i in this.before) {
                    if (this.beforeRegexp[i].test(funcname) === true) {
                    
                        var func = this[this.before[i]];
                        return (func ? func : false);
                    }
                }
            }
            return false;
        };

    
    return Fs.subclass({
        constructor: function() {
            _prepareBefore.call(this);
            if (this.routes) {
                for (var i in this.routes) {
                    this.route(i, this.routes[i]);
                }
            }
        },

        route: function(path, foo) {
            var before,
                callback = this[foo],
                self = this;

            if (callback) {
                path = routeToRegExp(path);
                before = _getBefore.call(self, foo);
                var cb = function(fragment) {
                    var args = extractParameters(path, fragment);
                    callback.apply(self, args);
                };
                if (before) {
                    Fs.History.route(path, function(fragment) {
                        before(function() {
                            return cb(fragment);
                        });
                    });
                } else {
                    Fs.History.route(path, cb);
                }
            }
            return self;
        }
    });
}());



Fs.Event = (function() {

    var _fs = Fs,
        _debug = _fs.debug,
        _utils = _fs.utils,
        _parent = _fs;

    function setupEvents() {
        _debug.log('event', 'setup');
        if (typeof this.events !== 'object') {
            
            
            this.events = {};
            this.uids = 0;
            
            
            this.pqueue = {};
            
            
            this.equeue = {};
        }
    };

    function _fire(name) {
        _debug.log('event', 'fire: ', this, arguments, this.events);
        var p = this.pqueue[name];

        if (p) {
            

            p = this.pqueue[name].slice(0);

            var e, events, elen, j, i = -1, plen = p.length,
                args = _utils.arraySlice(arguments, 1);

            while (++i < plen) {
                events = this.events[name]['p' + p[i]].slice(0);
                elen = events.length;
                j = -1;

                while (++j < elen) {
                    var e = this.equeue['u' + events[j]];

                    if (typeof e[2] !== 'function') {
                        console.warn('fire "', name, '" not a function: ',
                            arguments.callee.callee, e);
                    } else if (e[2].apply(e[3], args) === false) {
                        return false;
                    }
                }

            }
            return true;
        }
    };

    function _numSort(a, b) {
        return (b - a);
    };

    function _on(name, cb, scope, priority) {
        _debug.log('event', 'on: ', this, arguments, this.events);

        priority = priority || this.defaultPriority;
        if (typeof this.events[name] === 'undefined') {
            this.events[name] = {};
        }
        if (typeof this.events[name]['p' + priority] === 'undefined') {
            this.events[name]['p' + priority] = [];
        }
        if (typeof this.pqueue[name] === 'undefined') {
            this.pqueue[name] = [];
        }
        if (this.pqueue[name].indexOf(priority) === -1) {
            this.pqueue[name].push(priority);
            this.pqueue[name].sort(_numSort);
        }
        scope = scope || window;
        this.equeue['u' + (++this.uids)] = [name, priority, cb, scope];
        this.events[name]['p' + priority].push(this.uids);
        return this.uids;
    };

    function _countListener(name) {
        var e = this.events[name];

        if (e) {
            var total = 0,
                keys = Object.keys(this.events[name]),
                i = keys.length;

            while (i--) {
                total += e[keys[i]].length;
            }
            return total;
        }
        return 0;
    };

    function _off(uid) {
        _debug.log('event', 'off: ', arguments, uid, this.equeue['u' + uid]);
        var e = this.equeue['u' + uid];

        if (e) {
            var cb,
                length,
                name = e[0],
                priority = e[1],
                listeners = this.events[name]['p' + priority];

            listeners.splice(listeners.indexOf(uid), 1);
            length = listeners.length;

            delete this.equeue['u' + uid];

            if (!length) {
                
                delete this.events[name]['p' + priority];
                this.pqueue[name].splice(this.pqueue[name].indexOf(priority), 1);
                if (!this.pqueue[name].length) {
                    delete this.pqueue[name];
                    delete this.events[name];
                    return 0;
                }
            }
            return _countListener.call(this, name);
        }
        return false;
    };

    function _off_DEPRECATED(TOREMOVE, uid) {
        return _off.call(this, uid);
    };

    return _parent.subclass({

        
        priority: {
            CORE: 1000,
            VIEWS: 900,
            DEFAULT: 800
        },
        defaultPriority: 800,

        constructor: function(opts) {
            
            if (opts && opts.listeners) {
                this.listeners = opts.listeners;
                this.wasListened = {};
            }
            setupEvents.call(this);
            
        },

        setupListeners: function(eventNames, renderer) {
            if (this.listeners) {

                var name,
                    scope = this.listeners.scope || this,
                    i = eventNames.length;

                renderer = renderer || this;

                while (i--) {
                    name = eventNames[i];
                    if (this.listeners[name] &&
                        !this.wasListened[name]) {
                        this.wasListened[name] = true;
                        renderer.on(name, this.listeners[name], scope);
                    }
                }
            }
        },

        hasListener: function(name) {
            if (typeof this.listeners === 'object' &&
                typeof this.listeners[name] === 'function') {
                return true;
            }
            return false;
        },

        getListener: function(name) {
            return this.listeners[name];
        },

        fire: _fire,
        fireEvent: _fire, 

        on: _on,
        listenEvent: _on, 

        off: _off,
        removeListener: _off_DEPRECATED 
    });

}());


Fs.Templates = new (function() {
    
    var _fs = Fs,
        _handlebars = Handlebars,
        _request = _fs.Request;

    _handlebars.registerHelper('safe', function(str) {
        return new _handlebars.SafeString(str);
    });

    _handlebars.registerHelper('length', function(array) {
        return array.length;
    });

    _handlebars.registerHelper('geticon', function(icon) {
        return icon;
    });

    _handlebars.registerHelper('wordwrap', function(str, len) {
        if (str.length > len) {
            return str.substring(0, len) + '...';
        }
        return str;
    });

    _handlebars.registerHelper('substr', function(str, len) {
        if (str.length <= len) {
            return str;
        }
        return str.substring(0, len);
    });

    _handlebars.registerHelper('foreach', function(arr, options) {
        var length = arr.length;

        if (options.inverse && !length) {
            return options.inverse(this);
        }
        return arr.map(function(item, index) {
            if (typeof item !== 'object') {
                item = {
                    original: item
                };
            }
            item.$index = index;
            item.$first = index === 0;
            item.$last  = index === (length - 1);
            return options.fn(item);
        }).join('');
    });

    
    _handlebars.registerHelper('if_eq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context == options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    
    _handlebars.registerHelper('if_neq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context != options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    
    _handlebars.registerHelper('if_gteq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context >= options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    
    _handlebars.registerHelper('get', function(context, options) {
        var hash = options.hash;
        if (typeof context === 'undefined' ||
            (hash.choices &&
            hash.choices.indexOf(context) === -1)) {
            return hash['default'];
        }
        return context;
    });

    function loadtpl(name, cb) {
        if (_handlebars.templates[name]) {
            cb(_handlebars.templates[name]);
        } else {

            var url = this.path + name + '.handlebars';

            _request.ajax(url, {
                scope: this,
                callback: function(success, data) {
                    if (success) {
                        _handlebars.templates[name] = this.compile(data);
                        return cb(_handlebars.templates[name]);
                    }
                    console.warn('Error while loading template "' + url + '".');
                    return cb(false);
                }
            });
        }
    };

    function loadtpls(names, cb) {

        var results = {},
            i = names.length,
            remaining = names.length;

        while (i--) {
            loadtpl.call(this, names[i], (function(tplname) {

                return function(tpl) {
                    results[tplname] = tpl;
                    if (!(--remaining)) {
                        cb(results);
                    }
                };

            }(names[i])));
        }
    };

    
    return _fs.subclass({
        
        constructor: function() {
            this.path = '';
        },

        setPath: function(path) {
            this.path = path;
        },

        get: function(name) {
            return _handlebars.templates[name];
        },

        load: function(names, cb) {
            if (typeof names === 'object') {
                return loadtpls.apply(this, arguments);
            }
            return loadtpl.apply(this, arguments);
        },

        compile: function(source) {
            if (typeof source === 'function') {
                return source;
            } else if (typeof _handlebars.templates[source] === 'function') {
                return _handlebars.templates[source];
            } else if (_handlebars.compile) {
                return _handlebars.compile(source);
            }
            return function() { return source };
            
        }
    });
}())();


Fs.Settings = (function() {

    var _fs = Fs,
        _parent = _fs.Event;

    return _parent.subclass({

        constructor: function(config) {
            this.vars = config || {};
            _parent.prototype.constructor.apply(this, arguments);
        },

        set: function(name, value) {
            var oldValue = this.get(name);
            this.vars[name] = value;
            this.fire('settingchanged', this, name, value, oldValue);
            this.fire(name + 'changed', this, name, value, oldValue);
        },

        get: function(name, defaultValue) {
            if (this.vars[name]) {
                return this.vars[name];
            }
            return defaultValue;
        },

        gets: function() {
            var result = [],
                i = -1,
                length = arguments.length;

            while (++i < length) {
                result.push(this.get(arguments[i]));
            }
            return result;
        }
    });

}());

Fs.events.Abstract = (function() {

    var _fs = Fs,
        _debug = _fs.debug,
        _eventsAttached = {},
        _events = {},
        _noHandler = function() {
            _debug.warn(this.xtype, 'No handler is define !');
        };

    var _globalForwardHandler = function(name) {
        return function(e) {
            var handler = _events[name][e.target.id];
            if (handler) {
                handler(e);
            }
        };
    };

    var _globalAttach = function() {
        if (!_eventsAttached[this.eventName]) {
            _eventsAttached[this.eventName] = true;
            this.globalFwd.addEventListener(this.eventName,
                _globalForwardHandler(this.eventName), this.useCapture);
        }
        if (!this.el.id || !this.el.id.length) {
            this.el.id = _fs.Selector.generateId();
        }
        if (!_events[this.eventName]) {
            _events[this.eventName] = {};
        }
        _events[this.eventName][this.el.id] = this.handler;
    };

    var _globalDetach = function() {
        delete _events[this.eventName][this.el.id];
        if (_eventsAttached[this.eventName] &&
            !_events[this.eventName].length) {
            _eventsAttached[this.eventName] = false;
            this.globalFwd.removeEventListener(this.eventName,
                _globalForwardHandler, this.useCapture);
        }
    };

    return _fs.subclass({

        xtype: 'events.abstract',
        useCapture: false,
        autoAttach: false,
        
        eventName: false,
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,
        
        globalFwd: false,

        constructor: function(opts) {
            opts = opts || {};
            _fs.utils.apply(this, opts, [
                'useCapture',
                'autoAttach',
                'globalFwd'
            ]);
            if (!this.eventName && opts.eventName) {
                this.eventName = opts.eventName;
            }
            this.defaultEl = opts.el || window;
            this.defaultScope = opts.scope || this;
            this.defaultHandler = (opts.handler ?
                opts.handler.bind(this.defaultScope) : _noHandler);
            this.attached = false;
            if (this.autoAttach) {
                this.attach();
            }
        },

        attach: function(cmp, handler, scope) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            this.el = cmp || this.defaultEl;
            scope = scope || this.defaultScope;
            this.handler = (handler ?
                handler.bind(scope) : this.defaultHandler);
            if (this.eventName) {
                if (this.globalFwd) {
                    _globalAttach.call(this);
                } else {
                    this.el.addEventListener(this.eventName,
                        this.handler, this.useCapture);
                }
            }
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            this.el = cmp || this.el || this.defaultEl;
            this.handler = this.handler || this.defaultHandler;
            if (this.eventName) {
                if (this.globalFwd) {
                    _globalDetach.call(this);
                } else {
                    this.el.removeEventListener(this.eventName,
                        this.handler, this.useCapture);
                }
            }
        }
    });

})();

Fs.events.Click = (function() {

    
    var _fs = Fs,
        _fastclick = FastClick,
        _debug = _fs.debug,
        _isMobile = ('ontouchstart' in document.documentElement),
        _noHandler = function() {
            _debug.warn('click', 'No handler is define !');
        };

    window.addEventListener('load', function() {
        _fastclick.attach(document.body);
    }, false);

    function stopScroll(e) {
        e.preventDefault();
    };

    
    return _fs.subclass({

        xtype: 'events.click',
        defaultDisableScrolling: false,
        defaultUseCapture: false,
        autoAttach: false,
        
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,

        constructor: function(opts) {
            opts = opts || {};
            this.disableScrolling = opts.disableScrolling || this.defaultDisableScrolling;
            this.useCapture = opts.useCapture || this.defaultUseCapture;
            _fs.utils.apply(this, opts, [
                'autoAttach'
            ]);
            this.interval = opts.interval || this.defaultInterval;
            this.defaultEl = opts.el || window;
            this.defaultScope = opts.scope || this;
            this.defaultHandler = (opts.handler ?
                opts.handler.bind(this.defaultScope) : _noHandler);

            this.attached = false;
            if (this.autoAttach) {
                this.attach();
            }
        },

        attach: function(cmp, handler, scope) {

            if (this.attached === true) {
                return false;
            }

            this.attached = true;
            this.el = cmp || this.defaultEl;
            scope = scope || this.defaultScope;
            this.handler = (handler ?
                handler.bind(scope) : this.defaultHandler);

            this.el.addEventListener('click', this.handler, this.useCapture);
            if (this.disableScrolling) {
                this.el.addEventListener('touchmove', stopScroll, this.useCapture);
            }
        },

        detach: function(cmp) {

            if (this.attached === false) {
                return false;
            }

            this.attached = false;
            this.el = cmp || this.el || this.defaultEl;
            this.handler = this.handler || this.defaultHandler;

            this.el.removeEventListener('click', this.handler, this.useCapture);
            if (this.disableScrolling) {
                this.el.removeEventListener('touchmove', stopScroll, this.useCapture);
            }
        }
    });
}());


Fs.events.Scroll = (function() {

    
    var _lastEvent,
        _fs = Fs,
        _debug = _fs.debug,
        _window = window,
        _doc = document,
        _screenHeight = _doc.height,
        _eventsAttached = false,
        _scrolled = false,
        _events = new _fs.Event();

    var _noHandler = function() {
        _debug.error('scroll', '[Fs.events.Scroll] No handler is define !');
    };

    var _poolHandler = function() {
        if (_scrolled === true) {
            _scrolled = false;
            _events.fire('scroll', _lastEvent);
        }
    };

    var _windowScrollListener = function(event) {
        _scrolled = true;
        _lastEvent = event;
        _debug.log('scroll', 'window scroll', event, event.target, _events);
    };

    var _windowResizeListener = function(event) {
        _screenHeight = _doc.height;
        _debug.log('scroll', 'window resize', _screenHeight);
    };



    
    return _fs.subclass({

        xtype: 'events.scroll',
        defaultInterval: 500,
        useCapture: false,
        autoAttach: false,
        
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,

        constructor: function(opts) {
            opts = opts || {};
            _fs.utils.apply(this, opts, [
                'useCapture',
                'autoAttach'
            ]);
            this.interval = opts.interval || this.defaultInterval;
            this.defaultEl = opts.el || window;
            this.defaultScope = opts.scope || this;
            this.defaultHandler = (opts.handler ?
                opts.handler.bind(this.defaultScope) : _noHandler);
            this.attached = false;
            if (this.autoAttach) {
                this.attach();
            }
        },

        attach: function(cmp, handler, scope) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('scroll',
                    _windowScrollListener, false);
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _window.setInterval(_poolHandler, this.interval);
            }
            this.el = cmp || this.defaultEl;
            scope = scope || this.defaultScope;
            this.handler = (handler ?
                handler.bind(scope) : this.defaultHandler);
            this.euid = _events.on('scroll', this.handler, scope);
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            var length = _events.off(this.euid);
            this.el = cmp || this.el || this.defaultEl;
            this.handler = this.handler || this.defaultHandler;
            if (length === 0) {
                this.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('scroll',
                _windowScrollListener, false);
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_poolHandler);
        }
    });
})();



Fs.events.Resize = (function() {

    
    var _lastEvent,
        _window = window,
        _eventsAttached = false,
        _resized = false,
        _fs = Fs,
        _debug = _fs.debug,
        _events = new _fs.Event();

    var _noHandler = function() {
        _debug.warn('events.resize', 'No handler is define !');
    };

    var _poolHandler = function() {
        if (_resized === true) {
            _resized = false;
            _events.fire('resize', _lastEvent);
        }
    };

    var _windowResizeListener = function(event) {
        _resized = true;
        _lastEvent = event;
        _debug.log('events.resize', '[window] resize');
    };



    
    return _fs.subclass({

        defaultInterval: 500,

        constructor: function(opts) {
            opts = opts || {};
            this.interval = opts.interval || this.defaultInterval;
            this.attached = false;
        },

        attach: function(cmp, handler, scope) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _window.setInterval(_poolHandler, this.interval);
            }
            this.el = cmp;
            scope = scope || this;
            this.handler = handler.bind(scope) || _noHandler;
            this.euid = _events.on('resize', this.handler, scope);
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            this.el = cmp || this.el;
            var length = _events.off(this.euid);
            delete this.handler;
            if (length === 0) {
                this.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_poolHandler);
        }
    });
})();





Fs.events.Nobounce = (function() {

    
    var _lastEvent,
        _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _doc = document,
        _parent = _fs;

    var _isParentNoBounce = function(el, className) {
        while (el) {
            if (_selector.hasClass(el, className)) {
                return true;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _disableBounce = function(e) {
        _debug.log('nobounce', 'disable bounce ?');
        if (_isParentNoBounce(e.target, this.className)) {
            _debug.log('nobounce', 'disabling bounce !');
            e.preventDefault();
        }
    }

    
    return _parent.subclass({

        defaultClassName: 'nobounce',
        defaultAutoAttach: true,
        defaultEl: _doc,

        constructor: function(opts) {
            opts = opts || {};
            this.autoAttach = opts.autoAttach || this.defaultAutoAttach;
            this.className = opts.className || this.defaultClassName;
            this.el = opts.el || this.defaultEl;
            this.attached = false;
            this.handler = _disableBounce.bind(this);
            if (this.autoAttach === true) {
                this.attach(this.el);
            }
        },

        attach: function(cmp) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            this.el = cmp || this.el;
            this.el.addEventListener('touchmove', this.handler, false);
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            this.el = cmp || this.el;
            this.el.removeEventListener('touchmove', this.handler, false);
        }
    });
})();


Fs.events.Focus = (function() {

    
    var _fs = Fs,
        _parent = _fs.events.Abstract;

    
    return _parent.subclass({

        xtype: 'events.focus',
        eventName: 'focusin',
        globalFwd: window,

        constructor: _parent.prototype.constructor
    });
}());

Fs.events.Blur = (function() {

    
    var _fs = Fs,
        _parent = _fs.events.Abstract;

    
    return _parent.subclass({

        xtype: 'events.blur',
        eventName: 'focusout',
        globalFwd: window,

        constructor: _parent.prototype.constructor
    });
}());

Fs.events.Keypress = (function() {

    
    var _fs = Fs,
        _parent = _fs.events.Abstract;

    
    return _parent.subclass({

        xtype: 'events.keypress',
        eventName: 'keypress',
        globalFwd: window,

        constructor: function() {
            _parent.prototype.constructor.apply(this, arguments);
        }
    });
}());


Fs.data.Store = (function() {

    var _fs = Fs,
        _storage = _fs.Storage,
        _debug = _fs.debug,
        _fsrequest = _fs.Request,
        _parent = _fs.Event;

    return _parent.subclass({

        defaultEnableHashTable: false,
        defaultReaderRoot: 'data',
        defaultReaderTotal: 'total',
        defaultReaderSuccess: 'success',
        defaultReaderId: 'id',
        defaultStorageCache: false,

        constructor: function(config) {
            this.config = config || {};
            this.totalRecords = config.totalRecords || 0;
            this.loadedRecords = 0;
            this.records = [];
            this.isLoading = false;
            
            this.enableHashTable = config.enableHashTable || this.defaultEnableHashTable;
            this.storageCache = config.storageCache || this.defaultStorageCache;
            if (this.enableHashTable) {
                this.hashRecords = {};
            }
            this.rootProperty = this.getReaderRoot(this.defaultReaderRoot);
            this.totalProperty = this.getReaderTotal(this.defaultReaderTotal);
            this.successProperty = this.getReaderSuccess(this.defaultReaderSuccess);
            this.idProperty = this.getReaderId(this.defaultReaderId);
            _parent.prototype.constructor.call(this, arguments);
        },

        load: function(opts) {
            opts = _fs.utils.applyIfAuto(opts || {}, this.getProxyOpts());
            _debug.log('store', 'load/opts: ', opts);
            this.isLoading = true;
            var request,
                self = this,
                type = this.getProxyType(),
                finalCb = opts.callback;

            this.fire('beforeload', this, opts);
            var cb = function(success, data) {
                var total,
                    newRecords = [];

                _debug.log('store', 'callback', arguments);
                if (success && data[self.successProperty]) {
                    total = data[self.totalProperty];
                    if (typeof total !== 'undefined') {
                        self.totalRecords = total;
                    }
                    newRecords = data[self.rootProperty];
                    if (self.enableHashTable) {
                        var recordId, length = newRecords.length;
                        while (length--) {
                            recordId = newRecords[length][self.idProperty];
                            self.hashRecords['r' + recordId] = self.loadedRecords + length;
                        }
                    }
                    self.loadedRecords += newRecords.length;
                    self.records = self.records.concat(newRecords);
                } else {
                    self.fire('loaderror', success, self, data);
                }
                _debug.log('store', 'finalCb: ', finalCb);
                self.fire('load', success, self, newRecords);
                if (finalCb) {
                    finalCb(success, self, newRecords);
                }
                self.fire('afterload', success, self, newRecords);
                self.isLoading = false;
            };

            opts.url = opts.url || this.getProxyUrl();

            if (this.storageCache) {
                var key = JSON.stringify(_fs.utils.crc32(JSON.stringify(opts))),
                    results = _storage.getItem(key);
                if (results) {
                    setTimeout(function() {
                        cb(true, results);
                    }, 10);
                    return true;
                } else {
                    var tmp = cb;
                    cb = function(success, data) {
                        if (success && data[self.successProperty]) {
                            _storage.setItem(key, data);
                        }
                        tmp(success, data);
                    };
                }
            }

            if (this.getProxyCache() === false) {
                this.addUrlTimestamp(opts.url);
            }
            opts.callback = cb;
            if (type === 'jsonp') {
                request = _fsrequest.jsonp;
            } else if (type === 'memory') {
                request = _fsrequest.memory;
            } else {
                request = _fsrequest.ajax;
            }
            return request(opts.url, opts, this);
        },

        addRecord: function(record, fromStart) {
            var recordId = record[this.idProperty];
            if (this.enableHashTable) {
                this.hashRecords['r' + recordId] = this.loadedRecords;
            }
            if (fromStart === true) {
                this.records = [record].concat(this.records);
                if (this.enableHashTable) {
                    var rec,
                        i = this.loadedRecords + 1;
                    while (i--) {
                        rec = this.records[i];
                        this.hashRecords['r' + rec[this.idProperty]] = i;
                    }
                }
            } else {
                this.records = this.records.concat([record]);
            }
            this.loadedRecords += 1;
            this.totalRecords += 1;
            
        },

        reset: function() {
            this.records = [];
            this.totalRecords = 0;
            this.loadedRecords = 0;
        },

        addUrlTimestamp: function(url) {
            
            var date = new Date();
        },

        getRecord: function(recId) {
            
            if (!this.enableHashTable) {
                _debug.error('store', 'You must enable hash table for using store.getRecord by setting enableHashTable: true in store config.');
                return;
            }
            
            return this.records[this.hashRecords['r' + recId]];
        },

        getAt: function(index) {
            return this.records[index];
        },

        setProxyOpts: function(opts) {
            if (!this.config.proxy) {
                this.config.proxy = {};
            }
            this.config.proxy.opts = opts;
            _debug.warn('store', 'setProxyOpts', opts, this.config.proxy);
            return true;
        },

        getProxyOpts: function(defaultValue) {
            defaultValue = defaultValue || {};
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.opts || defaultValue;
            }
            return defaultValue;
        },

        getProxyUrl: function(defaultValue) {
            defaultValue = defaultValue || '';
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.url || defaultValue;
            }
            return defaultValue;
        },

        getProxyCache: function(defaultValue) {
            defaultValue = defaultValue || true;
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return (p.cache === false ? false : defaultValue);
            }
            return defaultValue;
        },

        getProxyType: function(defaultValue) {
            defaultValue = defaultValue || 'ajax';
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.type || defaultValue;
            }
            return defaultValue;
        },

        getReaderRoot: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderRoot;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.rootProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderTotal: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderTotal;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.totalProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderSuccess: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderSuccess;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.successProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderId: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderId;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.idProperty || defaultValue;
            }
            return defaultValue;
        }
    });
}());


Fs.data.PagingStore = (function() {

    var _fs = Fs,
        _parent = _fs.data.Store;

    return _parent.subclass({

        constructor: function(config) {
            config = config || {};
            this.currentPage = config.currentPage || 0;
            this.recordPerPage = config.recordPerPage || 10;
            _parent.prototype.constructor.call(this, config);
        },

        loadPage: function(page, opts) {
            _fs.debug.log('[pagingstore] !! opts', opts);
            opts = opts || {};
            opts.url = opts.url || this.getProxyUrl();
            var nb = this.recordPerPage,
                paging = 'start=' + (page * nb) +
                    '&limit=' + nb;
            if (opts.url.indexOf('?') !== -1) {
                opts.url += '&' + paging;
            } else {
                opts.url += '?' + paging;
            }
            _fs.debug.log('[pagingstore] loadPage', opts, arguments);
            return this.load(opts);
        },

        nextPage: function(opts) {
            _fs.debug.log('[pagingstore/nextPage] opts', opts);
            return this.loadPage(this.currentPage++, opts);
        },

        previousPage: function(opts) {
            return this.loadPage(this.currentPage--, opts);
        },

        reset: function() {
            this.currentPage = 0;
            _parent.prototype.reset.apply(this, arguments);
        }
    });

}());


Fs.views.Template = (function() {

    var _fs = Fs,
        _gconf = _fs.config,
        _parent = _fs.Event,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _win = window,
        
        j = 0;

    function _registerEngineEvents() {
        if (!this.renderer ||
            this.engineEventsRegistered) {
            return false;
        }
        this.engineEventsRegistered = true;

        var i, e, events = this.engine.getEvents(this.xtype);

        for (i in events) {
            e = events[i];
            if (typeof e === 'function') {
                if (i.indexOf('render') >= 0) {
                    this.renderer.on(i, events[i], this, this.priority.VIEWS);
                } else {
                    this.on(i, events[i], this, this.priority.VIEWS);
                }
            }
        }
        return true;
    };

    

    return _parent.subclass({

        className: 'Fs.views.Template',
        xtype: 'template',

        constructor: function(opts) {
            opts = opts || {
                template: '',
                config: {},
                items: []
            };
            this.config = opts.config || {};
            if (!this.config.id) {
                this.config.id = _selector.generateId();
            }
            this.items = opts.items || [];
            if (!this.items.indexOf) {
                this.items = [this.items];
            }
            this.data = this.config;
            this.data.items = this.items;
            if (!opts.template) {
                opts.template = this.setEngine(opts, _gconf).getTpl(this.xtpl || this.xtype);
            } else {
                this.setEngine(opts, _gconf);
            }
            if (!opts.ui && _gconf.ui) {
                opts.ui = _gconf.ui;
            }
            if (opts.ref) {
                this.ref = opts.ref;
            }
            this.tpl = opts.template;
            this.html = '';
            this.renderToEl = '';
            this.el = '';
            this.engineEventsRegistered = false;
            _parent.prototype.constructor.apply(this, arguments);
        },

        getEngine: function(config, gconfig) {
            if (config && config.engine) {
                return config.engine;
            } else if (gconfig) {
                return gconfig.engine;
            }
            return this.engine;
        },

        setEngine: function(config, gconfig) {
            this.engine = this.getEngine(config, gconfig);
            this.engine = new this.engine();
            return this.engine;
        },

        compile: function(parentConfig, renderer) {
            var item, tpl,
                i = -1,
                items = this.items,
                length = this.items.length,
                test = ++j;

            _debug.profile(this.xtype, 'compile' + test);
            if (parentConfig) {
                if (_gconf.ui && !parentConfig.ui) {
                    parentConfig.ui = _gconf.ui;
                }
                _fs.utils.applyIf(this.config, parentConfig, ['ui']);
            }

            while (++i < length) {
                item = items[i];
                if (typeof item === 'object' &&
                    typeof item.xtype === 'string' &&
                    typeof item.__proto__.xtype === 'undefined') {
                    item = new (_fs.xtype(item.xtype))(item);
                }
                if (typeof item === 'string') {
                    tpl = _fs.Templates.compile(item);
                    this.data.items[i] = tpl(this.config);
                } else if (typeof item.compile === 'function') {
                    this.data.items[i] = item.compile(this.config, renderer || this);
                } else {
                    this.data.items[i] = item;
                }
            }

            this.html = this.tpl(this.data);
            _debug.profileEnd(this.xtype, 'compile ' + test);
            if (!this.renderer) {
                this.renderer = renderer || this;
            }
            
            
            
            if (this.ref &&
                !this.renderer[this.ref]) {
                this.renderer[this.ref] = this;
            }
            

            this.__arID = this.renderer.on('afterrender', function() {
                this.el = document.getElementById(this.config.id);
                this.renderer.off(this.__arID);
                delete this.__arID;
            }, this, this.priority.CORE);

            this.setupListeners(['aftercompile', 'afterrender'], this.renderer);
            this.fire('aftercompile', this, this.renderer);
            _registerEngineEvents.call(this);
            return this.html;
        },

        
        render: function(renderToEl, position) {
            position = position || 'beforeEnd';
            if (typeof renderToEl === 'string') {
                
                this.renderToEl = _fs.Selector.get(renderToEl);
            } else {
                this.renderToEl = renderToEl;
            }
            
            
            
            
            

            this.renderToEl.insertAdjacentHTML(position, this.html);
            this.fire('afterrender', this);
        },

        setUI: function() {
            console.warn('setUI is not implemented for xtype "',
                this.xtype, '" and engine "', this.getEngine().xtype, '"');
        }
    });

}());


Fs.views.Abstract = (function() {

    
    var _fs = Fs,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Abstract',
        xtype: 'abstract',

        constructor: function(opts) {
            opts = opts || {};
            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtype = opts.xtpl;
            }
            _parent.prototype.constructor.call(this, opts);
        }
    });
})();



Fs.views.LoadMask = (function() {

    var _fs = Fs,
        _selector = _fs.Selector,
        _parent = Fs.views.Template;

    return _parent.subclass({

        className: 'Fs.views.LoadMask',
        xtype: 'loadmask',

        show: function() {
            _selector.removeClass(this.el, 'hidden');
        },

        hide: function() {
            _selector.addClass(this.el, 'hidden');
        }
    });

}());

Fs.views.Container = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Container',
        xtype: 'container'

    });
})();


Fs.views.Tabs = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Tabs',
        xtype: 'tabs'

    });
})();

Fs.views.Header = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Header',
        xtype: 'header'

    });
})();


Fs.views.Page = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Page',
        xtype: 'page'

    });
})();


Fs.views.Footer = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Footer',
        xtype: 'footer'

    });
})();


Fs.views.Toolbar = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Toolbar',
        xtype: 'toolbar'

    });
})();

Fs.views.Tabs = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Tabs',
        xtype: 'tabs'

    });
})();


Fs.views.Button = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        
        this.eventClick = new _fs.events.Click();
        this.eventClick.attach(this.el, this.handler, this);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    var _setDisabled = function(disabled) {
        disabled = (disabled === true);
        this.disabled = disabled;
        if (this.el) {
            if (disabled) {
                this.el.setAttribute('disabled', 'disabled');
            } else {
                this.el.removeAttribute('disabled');
            }
        }
        this.fire('disabled', this, disabled);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Button',
        xtype: 'button',

        constructor: function(opts) {
            opts = opts || {};
            if (opts.handler) {
                this.handler = (opts.scope ?
                    opts.handler.bind(opts.scope) : opts.handler);
            }
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        handler: function(event) {
            var route = this.config.route;
            
            _debug.log('button', 'handler: ', this, event, arguments, route);
            
            if (route) {
                _fs.History.navigate(route);
            }
        },

        setDisabled: _setDisabled
    });
})();



Fs.views.TabButton = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Button;

    
    return _parent.subclass({

        className: 'Fs.views.TabButton',
        xtype: 'tabbutton',

        handler: _parent.prototype.handler

    });
})();



Fs.views.List = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _parent = _fs.views.Template;

    

    var _findParentItem = function(el) {
        while (el) {
            if (_fs.Selector.hasClass(el, 'list-item')) {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _onAfterRender = function(cmp) {
        var id = this.config.id;

        this.loadingEl = document.getElementById(id + '-loader');
        if (this.hasListener('itemselect')) {
            this.clickEvent = new _fs.events.Click({
                autoAttach: true,
                el: this.el,
                scope: (this.listeners.scope ? this.listeners.scope : undefined),
                handler: this.getListener('itemselect')
            });
        }
        if (this.autoload) {
            this.load();
        }
        _debug.log('list', 'afterrender', arguments, this.config.id);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.List',
        xtype: 'list',

        defaultAutoload: true,

        constructor: function(opts) {
            opts = opts || {};
            this.store = opts.store;
            if (opts.listeners) {
                this.listeners = opts.listeners;
            }
            this.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : this.defaultAutoload);
            if (opts.config) {
                opts.config.isLoading = this.autoload;
            }
            this.emptyTpl = opts.emptyTpl || false;
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);

            this.itemTpl = opts.itemTpl || this.engine.getTpl(this.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = this.engine.getTpl(this.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    this.itemTpl = engineItemTpl;
                }
            }
        },

        getItemFromEvent: function(event) {
            return _findParentItem(event.target);
        },

        reload: function() {
            
            while (this.el.firstChild) {
                this.el.removeChild(this.el.firstChild);
            }
            if (this.store) {
                this.showLoading();
                this.store.load({
                    callback: this.storeLoaded.bind(this)
                });
            } else {
                this.loadItems(this.items);
            }
        },

        showLoading: function() {
            this.isLoading = true;
            Fs.Selector.removeClass(this.loadingEl, 'list-loader-hide');
        },

        hideLoading: function() {
            this.isLoading = false;
            Fs.Selector.addClass(this.loadingEl, 'list-loader-hide');
        },

        load: function(items) {
            if (this.isLoading === true) {
                return false;
            }
            if (this.store) {
                this.showLoading();
                this.store.load({
                    callback: this.storeLoaded.bind(this)
                });
            } else {
                this.loadItems(items || this.items);
            }
        },

        loadItems: function(items) {
            var itemTpl = this.itemTpl,
                config = _fs.utils.applyAuto(this.config, {});

            config.items = items;
            this.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            this.hideLoading();
        },

        storeLoaded: function(success, store, newRecords) {
            _debug.log('list', 'storeLoaded', arguments);

            this.loadItems(newRecords);

            if (!store.totalRecords && this.emptyTpl) {
                _selector.updateHtml(this.el, this.emptyTpl);
            }
        }
    });
})();




Fs.views.ListBuffered = (function() {

    
    var _window = window,
        _fs = Fs,
        _events = _fs.events,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;

    

    var _findParentItem = function(el) {
        while (el) {
            if (_selector.hasClass(el, 'list-item')) {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _onAfterRender = function(cmp) {
        this.loadingEl = this.el.parentNode.querySelector('.list-loader');
        if (this.hasListener('itemselect')) {
            this.clickEvent = new _events.Click({
                autoAttach: true,
                el: this.el,
                scope: (this.listeners.scope ? this.listeners.scope : undefined),
                handler: this.getListener('itemselect')
            });
        }
        this.scrollEvent = new _events.Scroll({
            autoAttach: true,
            el: this.el,
            scope: this,
            handler: this.scrollHandler
        });

        this.loadNext();

        this.renderer.on('show', function() {
            this.scrollEvent.attach();
            if (this.clickEvent) {
                this.clickEvent.attach();
            }
        }, this, this.priority.VIEWS);

        this.renderer.on('hide', function() {
            this.scrollEvent.detach();
            if (this.clickEvent) {
                this.clickEvent.detach();
            }
        }, this, this.priority.VIEWS);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.ListBuffered',
        xtype: 'listbuffered',

        constructor: function(opts) {
            opts = opts || {};
            this.store = opts.store;
            if (opts.listeners) {
                this.listeners = opts.listeners;
            }
            this.emptyTpl = opts.emptyTpl || false;
            this.isLoading = false;
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);

            this.itemTpl = opts.itemTpl || this.engine.getTpl(this.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = this.engine.getTpl(this.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    this.itemTpl = engineItemTpl;
                }
            }
        },

        getItemFromEvent: function(event) {
            return _findParentItem(event.target);
        },

        scrollHandler: function(event) {
            var p = document.body,
                
                screenHeight = _window.screen.availHeight,
                scrollHeight = p.scrollHeight,
                totalScroll = p.scrollTop + screenHeight;
            _debug.log('listbuffered', 'scroll', totalScroll, scrollHeight);
            if (totalScroll >= scrollHeight) {
                _debug.log('listbuffered', 'loading next');
                this.loadNext();
            }
        },

        reload: function() {
            if (this.isLoading === true) {
                return false;
            }
            this.isLoading = true;
            this.el.innerHTML = '';
            this.showLoading();
            this.store.reset();
            this.store.nextPage({
                callback: this.getNext.bind(this)
            });
        },

        loadNext: function() {
            if (this.isLoading === true) {
                return false;
            } else if (this.store.loadedRecords &&
                this.store.loadedRecords === this.store.totalRecords) {
                this.hideLoading();
                return false;
            }
            this.isLoading = true;
            this.showLoading();
            this.store.nextPage({
                callback: this.getNext.bind(this)
            });
        },

        getNext: function(success, store, newRecords) {
            var conf, itemTpl = this.itemTpl || _itemTpl;

            _debug.log('listbuffered', 'getNext', arguments);
            conf = this.config;
            conf.items = newRecords;
            _debug.log('listbuffered', 'records', newRecords);
            this.el.insertAdjacentHTML('beforeEnd', itemTpl(conf));
            
            
            
            _selector.redraw(this.el);
            
            if (store.loadedRecords === store.totalRecords) {
                this.hideLoading();
            }
            this.isLoading = false;

            if (!store.totalRecords) {
                this.scrollEvent.detach();
                if (this.emptyTpl) {
                    _selector.updateHtml(this.el, this.emptyTpl);
                }
            }
        },

        showLoading: function() {
            _selector.removeClass(this.loadingEl, 'list-loader-hide');
        },

        hideLoading: function() {
            _selector.addClass(this.loadingEl, 'list-loader-hide');
        }
    });
})();



Fs.views.Textfield = (function() {

    
    var _fs = Fs,
        
        _parent = _fs.views.Template;

    var _setDisabled = function(disabled) {
        disabled = (disabled === true);
        this.disabled = disabled;
        if (this.el) {
            if (disabled) {
                this.el.setAttribute('disabled', 'disabled');
            } else {
                this.el.removeAttribute('disabled');
            }
        }
        this.fire('disabled', this, disabled);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Textfield',
        xtype: 'textfield',

        setDisabled: _setDisabled
    });
})();


Fs.views.Passwordfield = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Textfield;

    
    return _parent.subclass({

        className: 'Fs.views.Passwordfield',
        xtype: 'passwordfield',
        xtpl: 'textfield',

        constructor: function(opts) {
            opts = opts || {};
            opts.config = opts.config || {};
            opts.config.type = 'password';
            opts.config.autocomplete = 'off';
            _parent.prototype.constructor.call(this, opts);
        }
    });
})();


Fs.views.FloatingPanel = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _parent = _fs.views.Template;

    var _onAfterRender = function(cmp) {
        
        this.overlayEl = document.getElementById(this.config.id + '-overlay');
        this.clickEvent = new _fs.events.Click({
            disableScrolling: true
        });
        this.clickEvent.attach(this.overlayEl, this.hide, this);
        this.resizeEvent = new _fs.events.Resize();
        this.resizeEvent.attach(this.el, this.resize, this);
        this.resize();
    };

    var _findParentItem = function(el) {
        while (el) {
            if (_fs.Selector.hasClass(el, 'floatingpanel')) {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _stopWindowScroll = function(e) {
        _debug.warn('floatingpanel', 'try to stop window scroll', e);
        if (_findParentItem(e.target) === false) {
            _debug.log('floatingpanel', ' stoping window scroll !', e);
            e.stopPropagation ? e.stopPropagation() : (e.cancelBubble = true);
            e.preventDefault ? e.preventDefault() : (e.returnValue = false);
            return false;
        }
    };

    var _realStopWindowScroll = function(e) {
        e.stopPropagation ? e.stopPropagation() : (e.cancelBubble = true);
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        return false;
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.FloatingPanel',
        xtype: 'floatingpanel',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);
            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        show: function() {
            _fs.Selector.removeClass(this.el, 'hidden');
            _fs.Selector.removeClass(this.overlayEl, 'hidden');
            _fs.Selector.addClass(document.body, 'scroll-stop');
            window.addEventListener('touchstart', _stopWindowScroll, false);
        },

        hide: function() {
            _fs.Selector.addClass(this.el, 'hidden');
            _fs.Selector.addClass(this.overlayEl, 'hidden');
            _fs.Selector.removeClass(document.body, 'scroll-stop');
            window.removeEventListener('touchstart', _stopWindowScroll, false);
        },

        resize: function(event) {
            if (!this.el.offsetWidth) {
                var self = this;
                setTimeout(function() {
                    self.resize();
                }, 100);
                return false;
            }

            var leftMargin,
                elWidth = this.el.offsetWidth,
                containerWidth = document.body.offsetWidth;

            leftMargin = (containerWidth - elWidth) / 2;
            this.el.style.left = leftMargin + 'px';
            return true;
        }
    });
})();



Fs.views.ControlGroup = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        
        this.eventClick = new Fs.events.Click();
        this.eventClick.attach(this.el, this.handler, this);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.ControlGroup',
        xtype: 'controlgroup',

        constructor: function(opts) {
            opts = opts || {};
            if (opts.handler) {
                this.handler = opts.handler;
            }
            _parent.prototype.constructor.call(this, opts);
            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        handler: function(event) {
            var route = this.config.route;
            _debug.log('controlgroup', 'handler: ', this, event, arguments, route);
            if (route) {
                _fs.History.navigate(route);
            }
        }
    });
})();



Fs.views.Collapsible = (function() {

    
    var _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        var engine = this.getEngine(),
            id = this.config.id;

        this.headerEl = document.getElementById(id + '-header');
        this.contentEl = document.getElementById(id + '-content');
        if (this.canCollapse) {
            this.eventClick = new Fs.events.Click({
                autoAttach: true,
                el: this.headerEl,
                scope: this,
                handler: this.handler
            });
        }
    };

    var _onAfterCompile = function() {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Collapsible',
        xtype: 'collapsible',

        constructor: function(opts) {
            opts = opts || {};
            opts.config = opts.config || {};
            this.collapsed = opts.config.collapsed || false;
            if (typeof opts.config.canCollapse !== 'undefined') {
                this.canCollapse = opts.config.canCollapse;
            } else {
                this.canCollapse = true;
            }
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        toggle: function() {
            if (this.collapsed) {
                return this.expand();
            }
            return this.collapse();
        },

        collapse: function() {
            this.collapsed = true;
            this.fire('collapse', this);
        },

        expand: function() {
            this.collapsed = false;
            this.fire('expand', this);
        },

        handler: function() {
            this.toggle();
        }
    });
})();



Fs.views.FlipSwitch = (function() {

    
    var _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        var id = this.config.id,
            engine = this.getEngine();

        
        this.uiEl = document.getElementById(id + '-ui');
        this.eventClick = new Fs.events.Click();
        this.eventClick.attach(this.uiEl, this.handler, this);
        if (this.config.value === this.config.options[1].value) {
            this.check();
        } else {
            this.uncheck();
        }
    };

    var _onAfterCompile = function() {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.FlipSwitch',
        xtype: 'flipswitch',

        constructor: function(opts) {
            opts = opts || {};
            opts.config = opts.config || {};
            if (!opts.config.options) {
                opts.config.options = [{
                    value: 'off',
                    label: 'Off'
                }, {
                    value: 'on',
                    label: 'On'
                }];
            }
            if (!opts.config.value) {
                opts.config.value = opts.config.options[0].value;
            }
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        toggle: function() {
            if (this.checked) {
                return this.uncheck();
            }
            return this.check();
        },

        check: function() {
            this.fire('check', this);
            this.checked = true;
            if (this.el) {
                this.el.selectedIndex = 1;
            }
        },

        uncheck: function() {
            this.fire('uncheck', this);
            this.checked = false;
            if (this.el) {
                this.el.selectedIndex = 0;
            }
        },

        handler: function() {
            this.toggle();
        }
    });
})();



Fs.views.Checkbox = (function() {

    
    var _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        var engine = this.getEngine();
        
        this.uiEl = document.getElementById(this.config.id + '-ui');
        this.eventClick = new Fs.events.Click();
        this.eventClick.attach(this.uiEl, this.handler, this);
        if (this.config.value) {
            this.check();
        } else {
            this.uncheck();
        }
    };

    var _onAfterCompile = function() {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Checkbox',
        xtype: 'checkbox',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        toggle: function() {
            if (this.checked) {
                return this.uncheck();
            }
            return this.check();
        },

        check: function() {
            this.fire('check', this);
            this.checked = true;
            if (this.el) {
                this.el.checked = true;
            }
        },

        uncheck: function() {
            this.fire('uncheck', this);
            this.checked = false;
            if (this.el) {
                this.el.checked = false;
            }
        },

        handler: function() {
            this.toggle();
        }
    });
})();



Fs.views.Textarea = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        if (this.clearBtn) {
            this.clearBtnEl = document.getElementById(this.config.id + '-clearBtn');
            
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Textarea',
        xtype: 'textarea',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        }
    });
})();



Fs.views.Select = (function() {

    
    var _fs = Fs,
        
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        var id = this.config.id;

        this.uiEl = document.getElementById(id + '-ui');
        this.setupListeners(['change']);
        this.changeEvent = new _fs.events.Abstract({
            autoAttach: true,
            eventName: 'change',
            el: this.el,
            scope: this,
            handler: function() {
                var item;

                if (this.store) {
                    item = this.store.getAt(this.el.selectedIndex);
                } else {
                    item = this.config.options[this.el.selectedIndex];
                }
                this.fire('change', this, item);
            }
        });
        if (this.store && this.autoload) {
            this.load();
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off('aftercompile', this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    var _setDisabled = function(disabled) {
        disabled = (disabled === true);
        this.disabled = disabled;
        if (this.el) {
            if (disabled) {
                this.el.setAttribute('disabled', 'disabled');
            } else {
                this.el.removeAttribute('disabled');
            }
        }
        this.fire('disabled', this, disabled);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Select',
        xtype: 'select',

        defaultAutoload: true,

        constructor: function(opts) {
            opts = opts || {};
            this.store = opts.store;
            opts.config = opts.config || {};
            this.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : this.defaultAutoload);

            _parent.prototype.constructor.call(this, opts);

            this.eid = this.off('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);

            this.itemTpl = opts.itemTpl || this.engine.getTpl(this.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = this.engine.getTpl(this.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    this.itemTpl = engineItemTpl;
                }
            }
        },

        setLoading: function(loading) {
            loading = (loading === true);
            this.isLoading = loading;
            this.fire('loading', this, loading);
        },

        setDisabled: _setDisabled,

        reload: function(items) {
            while (this.el.firstChild) {
                this.el.removeChild(this.el.firstChild);
            }
            if (this.store) {
                this.setLoading(true);
                this.store.reset();
                this.store.load({
                    callback: this.storeLoaded.bind(this)
                });
            } else {
                this.loadItems(items || this.config.options);
            }
        },

        clearSelection: function() {
            if (this.el) {
                var emptyItem = {};

                if (this.config.emptyText) {
                    emptyItem.label = this.config.emptyText;
                }
                this.el.selectedIndex = 0;
                this.fire('change', this, emptyItem);
            }
        },

        load: function(items) {
            if (this.isLoading === true) {
                return false;
            }
            if (this.store) {
                this.setLoading(true);
                this.store.load({
                    callback: this.storeLoaded.bind(this)
                });
            } else {
                this.loadItems(items || this.config.options);
            }
        },

        loadItems: function(items) {
            var itemTpl = this.itemTpl,
                config = _fs.utils.applyAuto(this.config, {});

            config.options = items;
            this.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            this.setLoading(false);
        },

        storeLoaded: function(success, store, newRecords) {
            if (this.config.emptyText) {
                store.addRecord({
                    label: this.config.emptyText
                }, true);
            }
            this.loadItems(this.store.records);
        }
    });
})();


Fs.views.RadioButton = (function() {

    
    var _radioUncheckEvent,
        _window = window,
        _fs = Fs,
        _selector = _fs.Selector,
        
        _parent = _fs.views.Template;

    
     if (_window.CustomEvent) {
        _radioUncheckEvent = new _window.CustomEvent('radioUncheck', {
            
            bubbles: true,
            cancelable: true
        });
    }

    var _findParentForm = function(el) {
        while (el) {
            if (el.tagName === 'FORM') {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _getOtherRadios = function(formEl, name) {
        return _selector.gets('input[name="' + name + '"]', formEl);
    };

    var _onAfterRender = function() {
        var id = this.config.id;

        this.uiEl = document.getElementById(id + '-ui');
        this.clickEvent = new _fs.events.Click();
        this.clickEvent.attach(this.uiEl, this.handler, this);
        this.changeEvent = new _fs.events.Abstract({
            autoAttach: true,
            eventName: 'radioUncheck',
            el: this.el,
            scope: this,
            handler: this.uncheck
        });
        if (this.config.checked) {
            this.check();
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.RadioButton',
        xtype: 'radiobutton',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        check: function() {
            if (this.checked) {
                return false;
            }
            this.fire('check', this);
            this.checked = true;
            if (this.el) {
                var item, form = _findParentForm(),
                    radios = _getOtherRadios(form, this.config.name),
                    i = radios.length;

                while (i--) {
                    item = radios[i];
                    if (item.checked && _radioUncheckEvent) {
                        item.dispatchEvent(_radioUncheckEvent);
                    }
                }
                this.el.checked = true;
            }
        },

        uncheck: function() {
            if (!this.checked) {
                return false;
            }
            this.fire('uncheck', this);
            this.checked = false;
            if (this.el) {
                this.el.checked = false;
            }
        },

        handler: function() {
            if (!this.checked) {
                this.check();
            }
        }
    });
})();



Fs.views.Popup = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;

    var _onAfterRender = function(cmp) {
        _debug.log('popup', 'afterrender', arguments, id);
        var id = this.config.id;

        this.overlayEl = document.getElementById(id + '-overlay');
        this.closeEl = document.getElementById(id + '-close');
        if (this.config.overlayHide !== false) {
            this.clickEvent = new _fs.events.Click({
                disableScrolling: true
            });
            this.clickEvent.attach(this.overlayEl, this.hide, this);
        }
        if (this.closeEl) {
            this.closeEvent = new _fs.events.Click({
                disableScrolling: true
            });
            this.closeEvent.attach(this.closeEl, this.hide, this);
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Popup',
        xtype: 'popup',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        show: function() {
            if (this.el) {
                _selector.removeClass(this.el, 'hidden');
                _selector.removeClass(this.overlayEl, 'hidden');
            }
        },

        hide: function() {
            if (this.el) {
                _selector.addClass(this.el, 'hidden');
                _selector.addClass(this.overlayEl, 'hidden');
            }
        }

    });
})();


Fs.views.Alert = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;

    var _onAfterRender = function(cmp) {
        _debug.log('alert', 'afterrender', arguments, id);
        var id = this.config.id;
        
        this.overlayEl = document.getElementById(id + '-overlay');
        this.closeEl = document.getElementById(id + '-close');
        this.clickEvent = new _fs.events.Click({
            disableScrolling: true
        });
        if (this.config.overlayHide !== false) {
            this.clickEvent.attach(this.overlayEl, this.hide, this);
        } else {
            this.clickEvent.attach(this.overlayEl, function() {}, this);
        }
        if (this.closeEl) {
            this.closeEvent = new _fs.events.Click({
                disableScrolling: true
            });
            this.closeEvent.attach(this.closeEl, this.hide, this);
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        _debug.log('alert', 'on aftercompile', arguments);
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Alert',
        xtype: 'alert',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        show: function() {
            if (this.el) {
                _selector.removeClass(this.el, 'hidden');
                _selector.removeClass(this.overlayEl, 'hidden');
            }
            this.fire('show', this);
        },

        hide: function() {
            if (this.el) {
                _selector.addClass(this.el, 'hidden');
                _selector.addClass(this.overlayEl, 'hidden');
            }
            this.fire('hide', this);
        }

    });
})();


Fs.views.Panel = (function() {

    
    var _fs = Fs,
        _selector = _fs.Selector,
        
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        var id = this.config.id;

        this.overlayEl = document.getElementById(id + '-overlay');
        this.clickEvent = new _fs.events.Click({
            disableScrolling: true
        });
        this.clickEvent.attach(this.overlayEl, function() {
            this.collapse();
        }, this);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Panel',
        xtype: 'panel',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        resize: function() {
            var height = window.innerHeight;

            this.el.offsetHeight = height;
            this.el.style.height = height + 'px';
        },

        expand: function() {
            var el = this.el,
                height = window.innerHeight;

            this.expanded = true;
            _selector.removeClass(this.overlayEl, 'hidden');
            this.el.addEventListener('webkitTransitionEnd', function() {
                el.offsetHeight = height;
                el.style.height = height + 'px';
                el.removeEventListener('webkitTransitionEnd',
                    arguments.callee, false);
            }, false);
            this.resizeEvent = new _fs.events.Resize();
            this.resizeEvent.attach(window, this.resize, this);
            this.fire('expand', this);
        },

        collapse: function() {
            this.expanded = false;
            if (this.resizeEvent) {
                this.resizeEvent.detach();
            }
            _selector.addClass(this.overlayEl, 'hidden');
            this.fire('collapse', this);
        }
    });
})();
Fs.views.AddToHome = (function() {

    var _fs = Fs,
        _priority = _fs.Event.prototype.priority,
        _parent = _fs.views.Template;

    function close() {
        if (this.timeout) {
            var timeout = (new Date()).getTime() + (this.timeout * 1000.0);

            _fs.Storage.setItem('addtohome', timeout, window.location.host);
        }
        _fs.Selector.addClass(this.el, 'hidden');
    };

    function afterrender() {
        this.closeEl = this.el.querySelector('.addtohome-close');
        this.clickEvent = new _fs.events.Click({
            autoAttach: true,
            el: this.closeEl,
            scope: this,
            handler: close
        });
    };

    function aftercompile(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', afterrender,
            this, _priority.VIEWS);
    };

    function canAddToHome() {
        if (window.navigator.standalone !== true &&
            (/iphone|ipod|ipad/gi).test(navigator.platform) &&
            (/Safari/i).test(navigator.appVersion)) {

            var isTimeout = _fs.Storage.getItem('addtohome', window.location.host),
                now = (new Date()).getTime();

            if (!isTimeout || isTimeout < now) {
                return true;
            }
        }
        return false;
    };

    window.canAddToHome = canAddToHome;

    return _parent.subclass({

        xtype: 'addtohome',
        className: 'Fs.views.AddToHome',

        constructor: function(opts) {
            opts = opts || {};
            this.renderTo = opts.renderTo || null;
            this.timeout = opts.timeout || false;

            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', aftercompile,
                this, _priority.VIEWS);

            if (this.renderTo) {
                this.compile();
                this.render(this.renderTo);
            }
        }

    })

}());!function(){var s=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.cssClass=s(function(s,a,e,n,r){function t(s){var a="";return a+=" "+o(typeof s===i?s.apply(s):s)}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,s.helpers),r=r||{};var l,p,i="function",o=this.escapeExpression,c=this,f=e.blockHelperMissing;return l=a.cssClass,l=typeof l===i?l.apply(a):l,p=f.call(a,l,{hash:{},inverse:c.noop,fn:c.program(1,t,r),data:r}),p||0===p?p:""})}();!function(){var e=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.for_id=e(function(e,a,r,n,t){function s(e){var a="";return a+=' for="'+o(typeof e===l?e.apply(e):e)+'"'}this.compilerInfo=[4,">= 1.0.0"],r=this.merge(r,e.helpers),t=t||{};var p,i,l="function",o=this.escapeExpression,f=this,c=r.blockHelperMissing;return p=a.id,p=typeof p===l?p.apply(a):p,i=c.call(a,p,{hash:{},inverse:f.noop,fn:f.program(1,s,t),data:t}),i||0===i?i:""})}();!function(){var e=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.id=e(function(e,a,n,r,t){function s(e){var a="";return a+=' id="'+o(typeof e===l?e.apply(e):e)+'"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,e.helpers),t=t||{};var i,p,l="function",o=this.escapeExpression,c=this,d=n.blockHelperMissing;return i=a.id,i=typeof i===l?i.apply(a):i,p=d.call(a,i,{hash:{},inverse:c.noop,fn:c.program(1,s,t),data:t}),p||0===p?p:""})}();!function(){var a=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.items=a(function(a,e,n,r,t){function s(a,e){var r;return r=n.safe.call(a,a,{hash:{},inverse:p.noop,fn:p.program(2,l,e),data:e}),r||0===r?r:""}function l(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),t=t||{};var i,o,p=this,f="function",c=n.blockHelperMissing;return i=e.items,i=typeof i===f?i.apply(e):i,o=c.call(e,i,{hash:{},inverse:p.noop,fn:p.program(1,s,t),data:t}),o||0===o?o:""})}();!function(){var e=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.style=e(function(e,a,t,s,n){function r(e){var a="";return a+=' style="'+o(typeof e===i?e.apply(e):e)+'"'}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,e.helpers),n=n||{};var l,p,i="function",o=this.escapeExpression,c=this,f=t.blockHelperMissing;return l=a.style,l=typeof l===i?l.apply(a):l,p=f.call(a,l,{hash:{},inverse:c.noop,fn:c.program(1,r,n),data:n}),p||0===p?p:""})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.addtohome=a(function(a,e,s,i,d){this.compilerInfo=[4,">= 1.0.0"],s=this.merge(s,a.helpers),i=this.merge(i,a.partials),d=d||{};var t,o,l="",n=this,m="function",c=this.escapeExpression;return l+='<div class="addtohome"',t=n.invokePartial(i.id,"id",e,s,i,d),(t||0===t)&&(l+=t),l+='><div class="addtohome-popup"><div class="addtohome-icon"><div class="addtohome-icon-image" style="background-image: url('+c((t=e.image,typeof t===m?t.apply(e):t))+');">&nbsp;</div>'+"</div>"+'<div class="addtohome-content">',o=n.invokePartial(i.items,"items",e,s,i,d),(o||0===o)&&(l+=o),l+='</div><div class="addtohome-close"><span class="addtohome-closebtn"></span></div></div></div>'})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.alert=a(function(a,e,n,r,t){function o(a){var e,n="";return n+=" floatingpanel-overlay-"+x((e=a.overlay,typeof e===k?e.apply(a):e))}function i(){return" floatingpanel-overlay-default"}function p(a){var e="";return e+=' id="'+x(typeof a===k?a.apply(a):a)+'-overlay"'}function l(a){var e="";return e+="ui-corner-"+x(typeof a===k?a.apply(a):a)}function s(a){var e="";return e+=" ui-body-"+x(typeof a===k?a.apply(a):a)}function u(){return" ui-overlay-shadow"}function c(a,e,n){var r,t,o="";return o+='<div class="ui-btn-'+x((r=a.close,typeof r===k?r.apply(a):r))+" ui-btn",r=n.ui,r=typeof r===k?r.apply(a):r,t=D.call(a,r,{hash:{},inverse:H.noop,fn:H.programWithDepth(14,f,e,a),data:e}),(t||0===t)&&(o+=t),r=n.shadow,r=typeof r===k?r.apply(a):r,t=D.call(a,r,{hash:{},inverse:H.noop,fn:H.program(16,h,e),data:e}),(t||0===t)&&(o+=t),o+=' ui-btn-corner-all ui-btn-icon-notext"',r=n.id,r=typeof r===k?r.apply(a):r,t=D.call(a,r,{hash:{},inverse:H.noop,fn:H.program(18,d,e),data:e}),(t||0===t)&&(o+=t),o+='><span class="ui-btn-inner"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></div>'}function f(a,e,r){var t,o="";return o+=" ui-btn-up-"+x(n.get.call(a,(t=r.header,null==t||t===!1?t:t.ui),{hash:{"default":a},data:e}))}function h(){return" ui-shadow"}function d(a){var e="";return e+=' id="'+x(typeof a===k?a.apply(a):a)+'-close"'}function v(a,e){var n,r,t="";return t+='<div class="ui-corner-top ui-header',n=a.ui,n=typeof n===k?n.apply(a):n,r=D.call(a,n,{hash:{},inverse:H.noop,fn:H.programWithDepth(21,y,e,a),data:e}),(r||0===r)&&(t+=r),t+='">',n=a.header,n=null==n||n===!1?n:n.title,n=typeof n===k?n.apply(a):n,r=D.call(a,n,{hash:{},inverse:H.noop,fn:H.program(23,m,e),data:e}),(r||0===r)&&(t+=r),t+="</div>"}function y(a,e,r){var t,o="";return o+=" ui-bar-"+x(n.get.call(a,(t=r.header,null==t||t===!1?t:t.ui),{hash:{"default":a},data:e}))}function m(a){var e="";return e+='<h1 class="ui-title">'+x(typeof a===k?a.apply(a):a)+"</h1>"}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),r=this.merge(r,a.partials),t=t||{};var g,b,w="",k="function",x=this.escapeExpression,H=this,D=n.blockHelperMissing;return w+='<div class="floatingpanel-overlay',g=n["if"].call(e,e.overlay,{hash:{},inverse:H.program(3,i,t),fn:H.program(1,o,t),data:t}),(g||0===g)&&(w+=g),w+='"',g=e.id,g=typeof g===k?g.apply(e):g,b=D.call(e,g,{hash:{},inverse:H.noop,fn:H.program(5,p,t),data:t}),(b||0===b)&&(w+=b),w+='></div><div class="ui-popup-container ui-popup-active"',b=H.invokePartial(r.style,"style",e,n,r,t),(b||0===b)&&(w+=b),b=H.invokePartial(r.id,"id",e,n,r,t),(b||0===b)&&(w+=b),w+='><div class="',g=e.corner,g=typeof g===k?g.apply(e):g,b=D.call(e,g,{hash:{},inverse:H.noop,fn:H.program(7,l,t),data:t}),(b||0===b)&&(w+=b),w+=" ui-popup",g=e.ui,g=typeof g===k?g.apply(e):g,b=D.call(e,g,{hash:{},inverse:H.noop,fn:H.program(9,s,t),data:t}),(b||0===b)&&(w+=b),g=e.shadow,g=typeof g===k?g.apply(e):g,b=D.call(e,g,{hash:{},inverse:H.noop,fn:H.program(11,u,t),data:t}),(b||0===b)&&(w+=b),w+='" style="border: 0px !important;">',b=n["if"].call(e,e.close,{hash:{},inverse:H.noop,fn:H.programWithDepth(13,c,t,e),data:t}),(b||0===b)&&(w+=b),b=n["if"].call(e,e.header,{hash:{},inverse:H.noop,fn:H.program(20,v,t),data:t}),(b||0===b)&&(w+=b),w+='<div class="ui-corner-bottom ui-content',g=e.ui,g=typeof g===k?g.apply(e):g,b=D.call(e,g,{hash:{},inverse:H.noop,fn:H.program(9,s,t),data:t}),(b||0===b)&&(w+=b),w+='">',b=H.invokePartial(r.items,"items",e,n,r,t),(b||0===b)&&(w+=b),w+="</div></div></div>"})}();!function(){var n=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.button=n(function(n,a,r,e,i){function t(n){var a="";return a+=" ui-btn-up-"+P(typeof n===H?n.apply(n):n)}function o(){return" ui-shadow"}function s(){return" ui-disabled"}function p(){return" ui-btn-corner-all"}function l(){return" ui-mini"}function c(){return" ui-mini ui-btn-icon-notext"}function u(){return" ui-btn-inline"}function f(n,a){var e;return e=r["if"].call(n,n.iconPos,{hash:{},inverse:k.program(18,m,a),fn:k.program(16,h,a),data:a}),e||0===e?e:""}function h(n){var a,r="";return r+=" ui-btn-icon-"+P((a=n.iconPos,typeof a===H?a.apply(n):a))}function m(){return" ui-btn-icon-left"}function d(n){var a="";return a+=' style="'+P(typeof n===H?n.apply(n):n)+'"'}function v(n){var a;return P((a=n.text,typeof a===H?a.apply(n):a))}function g(){return"&nbsp;"}function b(n,a){var e,i="";return i+='<span class="icon override-inline-btn-icon">',e=r.geticon.call(n,n,{hash:{},inverse:k.noop,fn:k.program(27,y,a),data:a}),(e||0===e)&&(i+=e),i+="</span>"}function y(){var n="";return n}this.compilerInfo=[4,">= 1.0.0"],r=this.merge(r,n.helpers),e=this.merge(e,n.partials),i=i||{};var q,x,_="",H="function",P=this.escapeExpression,k=this,C=r.blockHelperMissing;return _+='<a class="ui-btn',q=a.ui,q=typeof q===H?q.apply(a):q,x=C.call(a,q,{hash:{},inverse:k.noop,fn:k.program(1,t,i),data:i}),(x||0===x)&&(_+=x),q=a.shadow,q=typeof q===H?q.apply(a):q,x=C.call(a,q,{hash:{},inverse:k.noop,fn:k.program(3,o,i),data:i}),(x||0===x)&&(_+=x),x=r.if_eq.call(a,a.disabled,{hash:{compare:!0,"default":!1},inverse:k.noop,fn:k.program(5,s,i),data:i}),(x||0===x)&&(_+=x),x=r.if_eq.call(a,a.roundCorners,{hash:{compare:!0,"default":!0},inverse:k.noop,fn:k.program(7,p,i),data:i}),(x||0===x)&&(_+=x),x=r.if_eq.call(a,a.size,{hash:{compare:"medium"},inverse:k.noop,fn:k.program(9,l,i),data:i}),(x||0===x)&&(_+=x),x=r.if_eq.call(a,a.size,{hash:{compare:"small"},inverse:k.noop,fn:k.program(11,c,i),data:i}),(x||0===x)&&(_+=x),x=r.if_eq.call(a,a.inline,{hash:{compare:!0,"default":!0},inverse:k.noop,fn:k.program(13,u,i),data:i}),(x||0===x)&&(_+=x),x=r["if"].call(a,a.icon,{hash:{},inverse:k.noop,fn:k.program(15,f,i),data:i}),(x||0===x)&&(_+=x),x=k.invokePartial(e.cssClass,"cssClass",a,r,e,i),(x||0===x)&&(_+=x),_+='"',q=a.style,q=typeof q===H?q.apply(a):q,x=C.call(a,q,{hash:{},inverse:k.noop,fn:k.program(20,d,i),data:i}),(x||0===x)&&(_+=x),x=k.invokePartial(e.id,"id",a,r,e,i),(x||0===x)&&(_+=x),_+='><span class="ui-btn-inner"><span class="ui-btn-text">',x=r["if"].call(a,a.text,{hash:{},inverse:k.program(24,g,i),fn:k.program(22,v,i),data:i}),(x||0===x)&&(_+=x),_+="</span>",q=a.icon,q=typeof q===H?q.apply(a):q,x=C.call(a,q,{hash:{},inverse:k.noop,fn:k.program(26,b,i),data:i}),(x||0===x)&&(_+=x),_+="</span></a>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.checkbox=a(function(a,e,n,r,i){function t(){return"on"}function p(){return"off"}function o(a){var e="";return e+=" ui-btn-up-"+v(typeof a===b?a.apply(a):a)}function l(){return" ui-mini"}function s(){return" ui-fullsize"}function c(a){var e="";return e+=' id="'+v(typeof a===b?a.apply(a):a)+'-ui"'}function u(a){return v(typeof a===b?a.apply(a):a)}function f(a){var e="";return e+=' name="'+v(typeof a===b?a.apply(a):a)+'"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),r=this.merge(r,a.partials),i=i||{};var h,m,d="",b="function",v=this.escapeExpression,y=this,g=n.blockHelperMissing;return d+='<div class="ui-checkbox"><label class="ui-checkbox-',h=n.if_eq.call(e,e.value,{hash:{compare:!0,"default":!1},inverse:y.program(3,p,i),fn:y.program(1,t,i),data:i}),(h||0===h)&&(d+=h),d+=" ui-btn",h=e.ui,h=typeof h===b?h.apply(e):h,m=g.call(e,h,{hash:{},inverse:y.noop,fn:y.program(5,o,i),data:i}),(m||0===m)&&(d+=m),d+=" ui-btn-corner-all",m=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:y.program(9,s,i),fn:y.program(7,l,i),data:i}),(m||0===m)&&(d+=m),d+=' ui-btn-icon-left"',h=e.id,h=typeof h===b?h.apply(e):h,m=g.call(e,h,{hash:{},inverse:y.noop,fn:y.program(11,c,i),data:i}),(m||0===m)&&(d+=m),d+='><span class="ui-btn-inner"><span class="ui-btn-text">',h=e.label,h=typeof h===b?h.apply(e):h,m=g.call(e,h,{hash:{},inverse:y.noop,fn:y.program(13,u,i),data:i}),(m||0===m)&&(d+=m),d+='</span><span class="ui-icon ui-icon-checkbox-',m=n.if_eq.call(e,e.value,{hash:{compare:!0,"default":!1},inverse:y.program(3,p,i),fn:y.program(1,t,i),data:i}),(m||0===m)&&(d+=m),d+=' ui-icon-shadow">&nbsp;</span></span></label><input type="checkbox"',h=e.name,h=typeof h===b?h.apply(e):h,m=g.call(e,h,{hash:{},inverse:y.noop,fn:y.program(15,f,i),data:i}),(m||0===m)&&(d+=m),m=y.invokePartial(r.id,"id",e,n,r,i),(m||0===m)&&(d+=m),d+="></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.collapsible=a(function(a,e,n,l,r){function i(){var a="";return a+=" ui-collapsible-inset"}function t(a){var e="";return e+=" ui-corner-"+z(typeof a===P?a.apply(a):a)}function o(){var a="";return a+=" ui-collapsible-collapsed"}function p(){var a="";return a+=" ui-collapsible-heading-collapsed"}function s(a){var e="";return e+=' id="'+z(typeof a===P?a.apply(a):a)+'-header"'}function c(){var a="";return a+=" ui-mini"}function u(){var a="";return a+=" ui-fullsize"}function f(a){var e,n="";return n+=" ui-btn-up-"+z((e=a.header,e=null==e||e===!1?e:e.ui,typeof e===P?e.apply(a):e))}function d(a,e){var n,l,r="";return n=a.ui,n=typeof n===P?n.apply(a):n,l=j.call(a,n,{hash:{},inverse:E.noop,fn:E.program(18,h,e),data:e}),(l||0===l)&&(r+=l),r}function h(a){var e="";return e+=" ui-btn-up-"+z(typeof a===P?a.apply(a):a)}function v(a,e){var l="";return l+=z(n.get.call(a,a.iconCollapsed,{hash:{"default":"plus"},data:e}))}function m(a,e){var l="";return l+=z(n.get.call(a,a.iconExpanded,{hash:{"default":"minus"},data:e}))}function g(a){var e,n="";return n+=" ui-body-"+z((e=a.content,e=null==e||e===!1?e:e.ui,typeof e===P?e.apply(a):e))}function y(a,e){var n,l,r="";return n=a.ui,n=typeof n===P?n.apply(a):n,l=j.call(a,n,{hash:{},inverse:E.noop,fn:E.program(27,b,e),data:e}),(l||0===l)&&(r+=l),r}function b(a){var e="";return e+=" ui-body-"+z(typeof a===P?a.apply(a):a)}function q(){var a="";return a+=" ui-collapsible-content-collapsed"}function _(a){var e="";return e+=' id="'+z(typeof a===P?a.apply(a):a)+'-content"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),l=this.merge(l,a.partials),r=r||{};var H,k,x="",P="function",z=this.escapeExpression,E=this,j=n.blockHelperMissing;return x+='<div class="ui-collapsible',H=n.if_eq.call(e,e.inset,{hash:{compare:!0,"default":!1},inverse:E.noop,fn:E.program(1,i,r),data:r}),(H||0===H)&&(x+=H),H=e.corner,H=typeof H===P?H.apply(e):H,k=j.call(e,H,{hash:{},inverse:E.noop,fn:E.program(3,t,r),data:r}),(k||0===k)&&(x+=k),x+=" ui-collapsible-themed-content",k=n.if_eq.call(e,e.collapsed,{hash:{compare:!0,"default":!1},inverse:E.noop,fn:E.program(5,o,r),data:r}),(k||0===k)&&(x+=k),x+='"',k=E.invokePartial(l.id,"id",e,n,l,r),(k||0===k)&&(x+=k),x+='><h4 class="ui-collapsible-heading',k=n.if_eq.call(e,e.collapsed,{hash:{compare:!0,"default":!1},inverse:E.noop,fn:E.program(7,p,r),data:r}),(k||0===k)&&(x+=k),x+='"',H=e.id,H=typeof H===P?H.apply(e):H,k=j.call(e,H,{hash:{},inverse:E.noop,fn:E.program(9,s,r),data:r}),(k||0===k)&&(x+=k),x+='><a href="javascript:;" class="ui-collapsible-heading-toggle ui-btn',k=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:E.program(13,u,r),fn:E.program(11,c,r),data:r}),(k||0===k)&&(x+=k),k=n["if"].call(e,(H=e.header,null==H||H===!1?H:H.ui),{hash:{},inverse:E.program(17,d,r),fn:E.program(15,f,r),data:r}),(k||0===k)&&(x+=k),x+=" ui-btn-icon-"+z(n.get.call(e,e.iconPos,{hash:{"default":"left",choices:"left|right"},data:r}))+'">'+'<span class="ui-btn-inner">'+'<span class="ui-btn-text">'+z((H=e.header,H=null==H||H===!1?H:H.title,typeof H===P?H.apply(e):H))+"</span>"+'<span class="ui-icon ui-icon-shadow'+" ui-icon-",k=n["if"].call(e,e.collapsed,{hash:{},inverse:E.program(22,m,r),fn:E.program(20,v,r),data:r}),(k||0===k)&&(x+=k),x+='">&nbsp;</span></span></a></h4><div class="ui-collapsible-content',k=n["if"].call(e,(H=e.content,null==H||H===!1?H:H.ui),{hash:{},inverse:E.program(26,y,r),fn:E.program(24,g,r),data:r}),(k||0===k)&&(x+=k),k=n.if_eq.call(e,e.collapsed,{hash:{compare:!0,"default":!1},inverse:E.noop,fn:E.program(29,q,r),data:r}),(k||0===k)&&(x+=k),x+='"',H=e.id,H=typeof H===P?H.apply(e):H,k=j.call(e,H,{hash:{},inverse:E.noop,fn:E.program(31,_,r),data:r}),(k||0===k)&&(x+=k),x+=">",k=E.invokePartial(l.items,"items",e,n,l,r),(k||0===k)&&(x+=k),x+="</div></div>"})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.container=a(function(a,n,e,r,t){function p(){return" hidden"}function o(a){var n="";return n+=" "+u(typeof a===d?a.apply(a):a)}function s(a){var n="";return n+=' style="'+u(typeof a===d?a.apply(a):a)+'"'}function l(a){var n="";return n+=' id="'+u(typeof a===d?a.apply(a):a)+'"'}function i(a,n){var r,t="";return r=e.safe.call(a,a,{hash:{},inverse:v.noop,fn:v.program(10,f,n),data:n}),(r||0===r)&&(t+=r),t}function f(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,a.helpers),t=t||{};var c,h,y="",d="function",u=this.escapeExpression,v=this,m=e.blockHelperMissing;return y+='<div class="container',c=n.hidden,c=typeof c===d?c.apply(n):c,h=m.call(n,c,{hash:{},inverse:v.noop,fn:v.program(1,p,t),data:t}),(h||0===h)&&(y+=h),c=n.cssClass,c=typeof c===d?c.apply(n):c,h=m.call(n,c,{hash:{},inverse:v.noop,fn:v.program(3,o,t),data:t}),(h||0===h)&&(y+=h),y+='"',c=n.style,c=typeof c===d?c.apply(n):c,h=m.call(n,c,{hash:{},inverse:v.noop,fn:v.program(5,s,t),data:t}),(h||0===h)&&(y+=h),c=n.id,c=typeof c===d?c.apply(n):c,h=m.call(n,c,{hash:{},inverse:v.noop,fn:v.program(7,l,t),data:t}),(h||0===h)&&(y+=h),y+=">",c=n.items,c=typeof c===d?c.apply(n):c,h=m.call(n,c,{hash:{},inverse:v.noop,fn:v.program(9,i,t),data:t}),(h||0===h)&&(y+=h),y+="</div>"})}();!function(){var e=Handlebars.template,t=Handlebars.templates=Handlebars.templates||{};t.content=e(function(e,t,a,n,r){function s(e){var t="";return t+=' style="'+f(typeof e===c?e.apply(e):e)+'"'}function i(e){var t="";return t+=' id="'+f(typeof e===c?e.apply(e):e)+'"'}this.compilerInfo=[4,">= 1.0.0"],a=this.merge(a,e.helpers),n=this.merge(n,e.partials),r=r||{};var p,l,o="",c="function",f=this.escapeExpression,h=this,m=a.blockHelperMissing;return o+='<div class="ui-content"',p=t.style,p=typeof p===c?p.apply(t):p,l=m.call(t,p,{hash:{},inverse:h.noop,fn:h.program(1,s,r),data:r}),(l||0===l)&&(o+=l),p=t.id,p=typeof p===c?p.apply(t):p,l=m.call(t,p,{hash:{},inverse:h.noop,fn:h.program(3,i,r),data:r}),(l||0===l)&&(o+=l),o+=">",l=h.invokePartial(n.items,"items",t,a,n,r),(l||0===l)&&(o+=l),o+="</div>"})}();!function(){var n=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.controlgroup=n(function(n,a,r,o,i){function t(n){var a;return z((a=n.position,typeof a===x?a.apply(n):a))}function e(){return"horizontal"}function s(){return" ui-mini"}function p(n,a,o){var i,t,e="";return e+='<a class="ui-btn ',i=o.ui,i=typeof i===x?i.apply(n):i,t=q.call(n,i,{hash:{},inverse:H.noop,fn:H.program(8,l,a),data:a}),(t||0===t)&&(e+=t),e+=" ui-shadow ui-btn-corner-all",t=r["if"].call(n,n.icon,{hash:{},inverse:H.noop,fn:H.program(10,c,a),data:a}),(t||0===t)&&(e+=t),t=r.if_eq.call(n,o.size,{hash:{compare:"small"},inverse:H.noop,fn:H.program(13,f,a),data:a}),(t||0===t)&&(e+=t),t=r["if"].call(n,n.$first,{hash:{},inverse:H.noop,fn:H.program(15,h,a),data:a}),(t||0===t)&&(e+=t),t=r["if"].call(n,n.$last,{hash:{},inverse:H.noop,fn:H.program(17,v,a),data:a}),(t||0===t)&&(e+=t),e+='"><span class="ui-btn-inner">',i=n.text,i=typeof i===x?i.apply(n):i,t=q.call(n,i,{hash:{},inverse:H.noop,fn:H.program(19,m,a),data:a}),(t||0===t)&&(e+=t),i=n.icon,i=typeof i===x?i.apply(n):i,t=q.call(n,i,{hash:{},inverse:H.noop,fn:H.program(21,d,a),data:a}),(t||0===t)&&(e+=t),e+="</span></a>"}function l(n){var a="";return a+="ui-btn-up-"+z(typeof n===x?n.apply(n):n)}function c(n,a){var o;return o=r["if"].call(n,n.iconPos,{hash:{},inverse:H.noop,fn:H.program(11,u,a),data:a}),o||0===o?o:""}function u(n){var a,r="";return r+=" ui-btn-icon-"+z((a=n.iconPos,typeof a===x?a.apply(n):a))}function f(){return" ui-btn-icon-notext"}function h(){return" ui-first-child"}function v(){return" ui-last-child"}function m(n){var a="";return a+='<span class="ui-btn-text">'+z(typeof n===x?n.apply(n):n)+"</span>"}function d(n,a){var o,i="";return i+='<span class="ui-icon override-btn-icon icon">',o=r.geticon.call(n,n,{hash:{},inverse:H.noop,fn:H.program(22,g,a),data:a}),(o||0===o)&&(i+=o),i+="</span>"}function g(){var n="";return n}this.compilerInfo=[4,">= 1.0.0"],r=this.merge(r,n.helpers),o=this.merge(o,n.partials),i=i||{};var y,b="",x="function",z=this.escapeExpression,H=this,q=r.blockHelperMissing;return b+='<div class="ui-corner-all ui-controlgroup ui-controlgroup-',y=r["if"].call(a,a.position,{hash:{},inverse:H.program(3,e,i),fn:H.program(1,t,i),data:i}),(y||0===y)&&(b+=y),y=r.if_eq.call(a,a.size,{hash:{compare:"medium"},inverse:H.noop,fn:H.program(5,s,i),data:i}),(y||0===y)&&(b+=y),y=r.if_eq.call(a,a.size,{hash:{compare:"small"},inverse:H.noop,fn:H.program(5,s,i),data:i}),(y||0===y)&&(b+=y),b+='"',y=H.invokePartial(o.id,"id",a,r,o,i),(y||0===y)&&(b+=y),b+='><div class="ui-controlgroup-controls">',y=r.foreach.call(a,a.buttons,{hash:{},inverse:H.noop,fn:H.programWithDepth(7,p,i,a),data:i}),(y||0===y)&&(b+=y),b+="</div></div>"})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.flipswitch=a(function(a,n,e,r,i){function t(a,n){var i,t="";return t+='<label class="ui-slider"',i=H.invokePartial(r.for_id,"for_id",a,e,r,n),(i||0===i)&&(t+=i),t+=">"+q(typeof a===k?a.apply(a):a)+"</label>"}function l(a){var n="";return n+=' name="'+q(typeof a===k?a.apply(a):a)+'"'}function p(a){var n,e="";return e+='<option value="'+q((n=a.value,typeof n===k?n.apply(a):n))+'">'+q((n=a.label,typeof n===k?n.apply(a):n))+"</option>"}function o(a){var n="";return n+=' id="'+q(typeof a===k?a.apply(a):a)+'-ui"'}function s(a){var n="";return n+=" ui-btn-down-"+q(typeof a===k?a.apply(a):a)}function u(){return" ui-mini"}function c(a,n,r){var i,t="";return t+='<span class="ui-slider-label ui-slider-label-',i=e.if_eq.call(a,a.$index,{hash:{compare:0},inverse:H.program(16,d,n),fn:H.program(14,f,n),data:n}),(i||0===i)&&(t+=i),i=e.if_eq.call(a,a.$index,{hash:{compare:1},inverse:H.program(20,v,n),fn:H.program(18,h,n),data:n}),(i||0===i)&&(t+=i),t+=' ui-btn-corner-all" style="width: ',i=e.isFlipSwitchOptionActive.call(a,r.options,a.$index,r.value,{hash:{},inverse:H.program(24,y,n),fn:H.program(22,m,n),data:n}),(i||0===i)&&(t+=i),t+=';">'+q((i=a.label,typeof i===k?i.apply(a):i))+"</span>"}function f(){return"b"}function d(){return"a"}function h(){var a="";return a+=" ui-btn-active"}function v(){var a="";return a+=" ui-btn-down-c"}function m(){var a="";return a+="100%"}function y(){var a="";return a+="0%"}function b(a){var n="";return n+=" ui-btn-up-"+q(typeof a===k?a.apply(a):a)}function g(a){var n="";return n+=' id="'+q(typeof a===k?a.apply(a):a)+'-btn"'}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,a.helpers),r=this.merge(r,a.partials),i=i||{};var w,x,_="",H=this,k="function",q=this.escapeExpression,$=e.blockHelperMissing;return w=n.label,w=typeof w===k?w.apply(n):w,x=$.call(n,w,{hash:{},inverse:H.noop,fn:H.program(1,t,i),data:i}),(x||0===x)&&(_+=x),_+="<select",w=n.name,w=typeof w===k?w.apply(n):w,x=$.call(n,w,{hash:{},inverse:H.noop,fn:H.program(3,l,i),data:i}),(x||0===x)&&(_+=x),x=H.invokePartial(r.id,"id",n,e,r,i),(x||0===x)&&(_+=x),_+=' class="ui-slider-switch">',x=e.each.call(n,n.options,{hash:{},inverse:H.noop,fn:H.program(5,p,i),data:i}),(x||0===x)&&(_+=x),_+="</select><div ",w=n.id,w=typeof w===k?w.apply(n):w,x=$.call(n,w,{hash:{},inverse:H.noop,fn:H.program(7,o,i),data:i}),(x||0===x)&&(_+=x),_+='class="ui-slider ui-slider-switch',w=n.ui,w=typeof w===k?w.apply(n):w,x=$.call(n,w,{hash:{},inverse:H.noop,fn:H.program(9,s,i),data:i}),(x||0===x)&&(_+=x),x=e.if_eq.call(n,n.size,{hash:{compare:"small","default":"medium"},inverse:H.noop,fn:H.program(11,u,i),data:i}),(x||0===x)&&(_+=x),_+=' ui-btn-corner-all">',x=e.foreach.call(n,n.options,{hash:{},inverse:H.noop,fn:H.programWithDepth(13,c,i,n),data:i}),(x||0===x)&&(_+=x),_+='<div class="ui-slider-inneroffset"><a href="javascript:;" class="ui-slider-handle ui-slider-handle-snapping ui-btn',w=n.ui,w=typeof w===k?w.apply(n):w,x=$.call(n,w,{hash:{},inverse:H.noop,fn:H.program(26,b,i),data:i}),(x||0===x)&&(_+=x),_+='ui-shadow ui-btn-corner-all"',w=n.id,w=typeof w===k?w.apply(n):w,x=$.call(n,w,{hash:{},inverse:H.noop,fn:H.program(28,g,i),data:i}),(x||0===x)&&(_+=x),_+='style="left: ',x=e.isFlipSwitchOptionActive.call(n,n.options,0,n.value,{hash:{},inverse:H.program(22,m,i),fn:H.program(24,y,i),data:i}),(x||0===x)&&(_+=x),_+=';"><span class="ui-btn-inner"><span class="ui-btn-text"></span></span></a></div></div>'})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.floatingpanel=a(function(a,n,e,r,p){function o(){return" hidden"}function t(a){var n,e="";return e+=" floatingpanel-overlay-"+b((n=a.overlay,typeof n===m?n.apply(a):n))}function l(){return" floatingpanel-overlay-default"}function i(a){var n="";return n+=' id="'+b(typeof a===m?a.apply(a):a)+'-overlay"'}function f(a){var n="";return n+=" arrow-"+b(typeof a===m?a.apply(a):a)}function s(a){var n="";return n+=" floatingpanel-"+b(typeof a===m?a.apply(a):a)}function y(a){var n="";return n+=' style="'+b(typeof a===m?a.apply(a):a)+'"'}function v(a){var n="";return n+=' id="'+b(typeof a===m?a.apply(a):a)+'"'}function c(a,n){var r,p="";return r=e.safe.call(a,a,{hash:{},inverse:H.noop,fn:H.program(18,d,n),data:n}),(r||0===r)&&(p+=r),p}function d(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,a.helpers),p=p||{};var h,u,g="",m="function",b=this.escapeExpression,H=this,w=e.blockHelperMissing;return g+='<div class="floatingpanel-overlay',h=n.hidden,h=typeof h===m?h.apply(n):h,u=w.call(n,h,{hash:{},inverse:H.noop,fn:H.program(1,o,p),data:p}),(u||0===u)&&(g+=u),u=e["if"].call(n,n.overlay,{hash:{},inverse:H.program(5,l,p),fn:H.program(3,t,p),data:p}),(u||0===u)&&(g+=u),g+='"',h=n.id,h=typeof h===m?h.apply(n):h,u=w.call(n,h,{hash:{},inverse:H.noop,fn:H.program(7,i,p),data:p}),(u||0===u)&&(g+=u),g+='></div><div class="floatingpanel',h=n.arrow,h=typeof h===m?h.apply(n):h,u=w.call(n,h,{hash:{},inverse:H.noop,fn:H.program(9,f,p),data:p}),(u||0===u)&&(g+=u),h=n.hidden,h=typeof h===m?h.apply(n):h,u=w.call(n,h,{hash:{},inverse:H.noop,fn:H.program(1,o,p),data:p}),(u||0===u)&&(g+=u),h=n.ui,h=typeof h===m?h.apply(n):h,u=w.call(n,h,{hash:{},inverse:H.noop,fn:H.program(11,s,p),data:p}),(u||0===u)&&(g+=u),g+='"',h=n.style,h=typeof h===m?h.apply(n):h,u=w.call(n,h,{hash:{},inverse:H.noop,fn:H.program(13,y,p),data:p}),(u||0===u)&&(g+=u),h=n.id,h=typeof h===m?h.apply(n):h,u=w.call(n,h,{hash:{},inverse:H.noop,fn:H.program(15,v,p),data:p}),(u||0===u)&&(g+=u),g+=">",h=n.items,h=typeof h===m?h.apply(n):h,u=w.call(n,h,{hash:{},inverse:H.noop,fn:H.program(17,c,p),data:p}),(u||0===u)&&(g+=u),g+="</div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.footer=a(function(a,e,r,n,p){function o(a){var e="";return e+=" ui-bar-"+d(typeof a===v?a.apply(a):a)}function t(a){var e="";return e+=" "+d(typeof a===v?a.apply(a):a)}function s(a){var e="";return e+=" ui-footer-"+d(typeof a===v?a.apply(a):a)}function l(a){var e="";return e+=' style="'+d(typeof a===v?a.apply(a):a)+'"'}function i(a){var e="";return e+=' id="'+d(typeof a===v?a.apply(a):a)+'"'}function f(a,e){var n,p="";return n=r.safe.call(a,a,{hash:{},inverse:m.noop,fn:m.program(12,y,e),data:e}),(n||0===n)&&(p+=n),p}function y(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],r=this.merge(r,a.helpers),p=p||{};var c,u,h="",v="function",d=this.escapeExpression,m=this,g=r.blockHelperMissing;return h+='<div class="ui-footer',c=e.ui,c=typeof c===v?c.apply(e):c,u=g.call(e,c,{hash:{},inverse:m.noop,fn:m.program(1,o,p),data:p}),(u||0===u)&&(h+=u),c=e.cssClass,c=typeof c===v?c.apply(e):c,u=g.call(e,c,{hash:{},inverse:m.noop,fn:m.program(3,t,p),data:p}),(u||0===u)&&(h+=u),c=e.position,c=typeof c===v?c.apply(e):c,u=g.call(e,c,{hash:{},inverse:m.noop,fn:m.program(5,s,p),data:p}),(u||0===u)&&(h+=u),h+='"',c=e.style,c=typeof c===v?c.apply(e):c,u=g.call(e,c,{hash:{},inverse:m.noop,fn:m.program(7,l,p),data:p}),(u||0===u)&&(h+=u),c=e.id,c=typeof c===v?c.apply(e):c,u=g.call(e,c,{hash:{},inverse:m.noop,fn:m.program(9,i,p),data:p}),(u||0===u)&&(h+=u),h+=">",c=e.items,c=typeof c===v?c.apply(e):c,u=g.call(e,c,{hash:{},inverse:m.noop,fn:m.program(11,f,p),data:p}),(u||0===u)&&(h+=u),h+="</div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.header=a(function(a,e,p,r,n){function t(a){var e="";return e+=" ui-bar-"+m(typeof a===d?a.apply(a):a)}function o(a){var e="";return e+=" "+m(typeof a===d?a.apply(a):a)}function l(a){var e="";return e+=" ui-header-"+m(typeof a===d?a.apply(a):a)}function s(a){var e="";return e+=' style="'+m(typeof a===d?a.apply(a):a)+'"'}function i(a){var e="";return e+=' id="'+m(typeof a===d?a.apply(a):a)+'"'}function f(a){var e="";return e+='<h1 class="ui-title">'+m(typeof a===d?a.apply(a):a)+"</h1>"}function y(a,e){var r,n="";return r=p.safe.call(a,a,{hash:{},inverse:g.noop,fn:g.program(14,h,e),data:e}),(r||0===r)&&(n+=r),n}function h(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],p=this.merge(p,a.helpers),n=n||{};var c,u,v="",d="function",m=this.escapeExpression,g=this,b=p.blockHelperMissing;return v+='<div class="ui-header',c=e.ui,c=typeof c===d?c.apply(e):c,u=b.call(e,c,{hash:{},inverse:g.noop,fn:g.program(1,t,n),data:n}),(u||0===u)&&(v+=u),c=e.cssClass,c=typeof c===d?c.apply(e):c,u=b.call(e,c,{hash:{},inverse:g.noop,fn:g.program(3,o,n),data:n}),(u||0===u)&&(v+=u),c=e.position,c=typeof c===d?c.apply(e):c,u=b.call(e,c,{hash:{},inverse:g.noop,fn:g.program(5,l,n),data:n}),(u||0===u)&&(v+=u),v+='"',c=e.style,c=typeof c===d?c.apply(e):c,u=b.call(e,c,{hash:{},inverse:g.noop,fn:g.program(7,s,n),data:n}),(u||0===u)&&(v+=u),c=e.id,c=typeof c===d?c.apply(e):c,u=b.call(e,c,{hash:{},inverse:g.noop,fn:g.program(9,i,n),data:n}),(u||0===u)&&(v+=u),v+=">",c=e.title,c=typeof c===d?c.apply(e):c,u=b.call(e,c,{hash:{},inverse:g.noop,fn:g.program(11,f,n),data:n}),(u||0===u)&&(v+=u),c=e.items,c=typeof c===d?c.apply(e):c,u=b.call(e,c,{hash:{},inverse:g.noop,fn:g.program(13,y,n),data:n}),(u||0===u)&&(v+=u),v+="</div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.list=a(function(a,e,r,n,t){function o(){return" ui-listview-inset"}function p(a){var e="";return e+=" ui-corner-"+m(typeof a===v?a.apply(a):a)}function l(){return" ui-shadow"}function i(a){var e="";return e+=" "+m(typeof a===v?a.apply(a):a)}function s(a){var e="";return e+=' style="'+m(typeof a===v?a.apply(a):a)+'"'}function f(a){var e="";return e+=' id="'+m(typeof a===v?a.apply(a):a)+'-loader"'}function c(){return" list-loader-hide"}function d(a){var e="";return e+=" list-loader-"+m(typeof a===v?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],r=this.merge(r,a.helpers),n=this.merge(n,a.partials),t=t||{};var u,y,h="",v="function",m=this.escapeExpression,g=this,b=r.blockHelperMissing;return h+='<ul class="ui-listview',u=e.inset,u=typeof u===v?u.apply(e):u,y=b.call(e,u,{hash:{},inverse:g.noop,fn:g.program(1,o,t),data:t}),(y||0===y)&&(h+=y),u=e.corner,u=typeof u===v?u.apply(e):u,y=b.call(e,u,{hash:{},inverse:g.noop,fn:g.program(3,p,t),data:t}),(y||0===y)&&(h+=y),u=e.shadow,u=typeof u===v?u.apply(e):u,y=b.call(e,u,{hash:{},inverse:g.noop,fn:g.program(5,l,t),data:t}),(y||0===y)&&(h+=y),u=e.cssClass,u=typeof u===v?u.apply(e):u,y=b.call(e,u,{hash:{},inverse:g.noop,fn:g.program(7,i,t),data:t}),(y||0===y)&&(h+=y),h+='"',u=e.style,u=typeof u===v?u.apply(e):u,y=b.call(e,u,{hash:{},inverse:g.noop,fn:g.program(9,s,t),data:t}),(y||0===y)&&(h+=y),y=g.invokePartial(n.id,"id",e,r,n,t),(y||0===y)&&(h+=y),h+="></ul><div ",u=e.id,u=typeof u===v?u.apply(e):u,y=b.call(e,u,{hash:{},inverse:g.noop,fn:g.program(11,f,t),data:t}),(y||0===y)&&(h+=y),h+=' class="list-loader',y=r.if_eq.call(e,e.isLoading,{hash:{compare:!1,defaults:!0},inverse:g.noop,fn:g.program(13,c,t),data:t}),(y||0===y)&&(h+=y),u=e.ui,u=typeof u===v?u.apply(e):u,y=b.call(e,u,{hash:{},inverse:g.noop,fn:g.program(15,d,t),data:t}),(y||0===y)&&(h+=y),h+='"><a class="list-loader-text">Loading...</a></div>'})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.listbuffered=a(function(a,e,n,r,t){function p(){return" ui-listview-inset"}function o(a){var e="";return e+=" ui-listview-corner-"+b(typeof a===g?a.apply(a):a)}function l(){return" ui-shadow"}function i(a){var e="";return e+=" "+b(typeof a===g?a.apply(a):a)}function s(a){var e="";return e+=' style="'+b(typeof a===g?a.apply(a):a)+'"'}function f(a){var e="";return e+=' id="'+b(typeof a===g?a.apply(a):a)+'"'}function u(a,e,r){var t,p,o="";return o+='<li class="ui-btn ui-li',t=r.ui,t=typeof t===g?t.apply(a):t,p=H.call(a,t,{hash:{},inverse:w.noop,fn:w.program(14,c,e),data:e}),(p||0===p)&&(o+=p),o+='">',p=n.safe.call(a,a,{hash:{},inverse:w.noop,fn:w.program(16,h,e),data:e}),(p||0===p)&&(o+=p),o+="</li>"}function c(a){var e="";return e+=" ui-btn-up-"+b(typeof a===g?a.apply(a):a)}function h(){var a="";return a}function y(a){var e="";return e+=" list-loader-"+b(typeof a===g?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),t=t||{};var v,d,m="",g="function",b=this.escapeExpression,w=this,H=n.blockHelperMissing;return m+='<ul class="ui-listview',v=e.inset,v=typeof v===g?v.apply(e):v,d=H.call(e,v,{hash:{},inverse:w.noop,fn:w.program(1,p,t),data:t}),(d||0===d)&&(m+=d),v=e.corner,v=typeof v===g?v.apply(e):v,d=H.call(e,v,{hash:{},inverse:w.noop,fn:w.program(3,o,t),data:t}),(d||0===d)&&(m+=d),v=e.shadow,v=typeof v===g?v.apply(e):v,d=H.call(e,v,{hash:{},inverse:w.noop,fn:w.program(5,l,t),data:t}),(d||0===d)&&(m+=d),v=e.cssClass,v=typeof v===g?v.apply(e):v,d=H.call(e,v,{hash:{},inverse:w.noop,fn:w.program(7,i,t),data:t}),(d||0===d)&&(m+=d),m+='"',v=e.style,v=typeof v===g?v.apply(e):v,d=H.call(e,v,{hash:{},inverse:w.noop,fn:w.program(9,s,t),data:t}),(d||0===d)&&(m+=d),v=e.id,v=typeof v===g?v.apply(e):v,d=H.call(e,v,{hash:{},inverse:w.noop,fn:w.program(11,f,t),data:t}),(d||0===d)&&(m+=d),m+=">",d=n.each.call(e,e.items,{hash:{},inverse:w.noop,fn:w.programWithDepth(13,u,t,e),data:t}),(d||0===d)&&(m+=d),m+='</ul><div class="list-loader',v=e.ui,v=typeof v===g?v.apply(e):v,d=H.call(e,v,{hash:{},inverse:w.noop,fn:w.program(18,y,t),data:t}),(d||0===d)&&(m+=d),m+='"><a class="list-loader-text">Loading...</a></div>'})}();!function(){var e=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.listbuffereditem=e(function(e,a,n,t,r){function i(e,a,t){var r,i,p="";return p+='<li class="ui-btn ui-li',r=t.ui,r=typeof r===u?r.apply(e):r,i=h.call(e,r,{hash:{},inverse:c.noop,fn:c.program(2,s,a),data:a}),(i||0===i)&&(p+=i),p+='">',i=n.safe.call(e,e,{hash:{},inverse:c.noop,fn:c.program(4,l,a),data:a}),(i||0===i)&&(p+=i),p+="</li>"}function s(e){var a="";return a+=" ui-btn-up-"+f(typeof e===u?e.apply(e):e)}function l(){var e="";return e}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,e.helpers),r=r||{};var p,o="",u="function",f=this.escapeExpression,c=this,h=n.blockHelperMissing;return p=n.each.call(a,a.items,{hash:{},inverse:c.noop,fn:c.programWithDepth(1,i,r,a),data:r}),(p||0===p)&&(o+=p),o})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.listitembasic=a(function(a,e,t,i,n){function r(a,e,i){var n,r,c="";return c+='<li class="list-item ui-li ui-li-static',n=t.if_eq.call(a,a.$first,{hash:{compare:!0},inverse:m.noop,fn:m.program(2,l,e),data:e}),(n||0===n)&&(c+=n),n=t.if_eq.call(a,a.$last,{hash:{compare:!0},inverse:m.noop,fn:m.program(4,s,e),data:e}),(n||0===n)&&(c+=n),n=i.ui,n=typeof n===u?n.apply(a):n,r=d.call(a,n,{hash:{},inverse:m.noop,fn:m.program(6,o,e),data:e}),(r||0===r)&&(c+=r),c+='">',n=a.label,n=typeof n===u?n.apply(a):n,r=d.call(a,n,{hash:{},inverse:m.noop,fn:m.program(8,p,e),data:e}),(r||0===r)&&(c+=r),c+="</li>"}function l(){return" ui-first-child"}function s(){return" ui-last-child"}function o(a){var e="";return e+=" ui-btn-up-"+h(typeof a===u?a.apply(a):a)}function p(a){return h(typeof a===u?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,a.helpers),n=n||{};var c,f="",u="function",h=this.escapeExpression,m=this,d=t.blockHelperMissing;return c=t.foreach.call(e,e.items,{hash:{},inverse:m.noop,fn:m.programWithDepth(1,r,n,e),data:n}),(c||0===c)&&(f+=c),f})}();!function(){var a=Handlebars.template,i=Handlebars.templates=Handlebars.templates||{};i.listitembutton=a(function(a,i,n,e,t){function r(a,i,e){var t,r="";return t=n.if_eq.call(a,a.type,{hash:{compare:"divider"},inverse:D.programWithDepth(16,d,i,e),fn:D.programWithDepth(2,l,i,e),data:i}),(t||0===t)&&(r+=t),r}function l(a,i,e){var t,r,l="";return l+='<li class="ui-li ui-li-divider',t=e.ui,t=typeof t===y?t.apply(a):t,r=W.call(a,t,{hash:{},inverse:D.noop,fn:D.programWithDepth(3,o,i,a),data:i}),(r||0===r)&&(l+=r),t=a.bubble,t=typeof t===y?t.apply(a):t,r=W.call(a,t,{hash:{},inverse:D.noop,fn:D.program(5,p,i),data:i}),(r||0===r)&&(l+=r),r=n.if_eq.call(a,a.$first,{hash:{compare:!0},inverse:D.noop,fn:D.program(7,s,i),data:i}),(r||0===r)&&(l+=r),r=n.if_eq.call(a,a.$last,{hash:{compare:!0},inverse:D.noop,fn:D.program(9,h,i),data:i}),(r||0===r)&&(l+=r),l+='">',t=a.label,t=typeof t===y?t.apply(a):t,r=W.call(a,t,{hash:{},inverse:D.noop,fn:D.program(11,u,i),data:i}),(r||0===r)&&(l+=r),r=n["if"].call(a,a.bubble,{hash:{},inverse:D.noop,fn:D.programWithDepth(13,c,i,e),data:i}),(r||0===r)&&(l+=r),l+="</li>"}function o(a,i,e){var t="";return t+=" ui-bar-"+g(n.get.call(a,e.ui,{hash:{"default":a},data:i}))}function p(){return" ui-li-has-count"}function s(){return" ui-first-child"}function h(){return" ui-last-child"}function u(a){return g(typeof a===y?a.apply(a):a)}function c(a,i,n){var e,t,r="";return r+='<span class="ui-li-count',e=n.ui,e=typeof e===y?e.apply(a):e,t=W.call(a,e,{hash:{},inverse:D.noop,fn:D.programWithDepth(14,f,i,a),data:i}),(t||0===t)&&(r+=t),r+=' ui-btn-corner-all">'+g((e=a.bubble,typeof e===y?e.apply(a):e))+"</span>"}function f(a,i,e){var t="";return t+=" ui-btn-up-"+g(n.get.call(a,e.ui,{hash:{"default":a},data:i}))}function d(a,i,e){var t,r,l="";return l+='<li class="list-item ui-btn',t=e.ui,t=typeof t===y?t.apply(a):t,r=W.call(a,t,{hash:{},inverse:D.noop,fn:D.programWithDepth(14,f,i,a),data:i}),(r||0===r)&&(l+=r),l+=" ui-btn-icon-right ui-li-has-arrow ui-li",t=a.bubble,t=typeof t===y?t.apply(a):t,r=W.call(a,t,{hash:{},inverse:D.noop,fn:D.program(5,p,i),data:i}),(r||0===r)&&(l+=r),r=n.if_eq.call(a,a.$first,{hash:{compare:!0},inverse:D.noop,fn:D.program(7,s,i),data:i}),(r||0===r)&&(l+=r),r=n.if_eq.call(a,a.$last,{hash:{compare:!0},inverse:D.noop,fn:D.program(9,h,i),data:i}),(r||0===r)&&(l+=r),l+='"><div class="ui-btn-inner ui-li"><div class="ui-btn-text"><a href="javascript:;" class="ui-link-inherit">',t=a.label,t=typeof t===y?t.apply(a):t,r=W.call(a,t,{hash:{},inverse:D.noop,fn:D.program(11,u,i),data:i}),(r||0===r)&&(l+=r),r=n["if"].call(a,a.bubble,{hash:{},inverse:D.noop,fn:D.programWithDepth(13,c,i,e),data:i}),(r||0===r)&&(l+=r),l+='</a></div><span class="ui-icon',t=e.icon,t=typeof t===y?t.apply(a):t,r=W.call(a,t,{hash:{},inverse:D.noop,fn:D.programWithDepth(17,v,i,a),data:i}),(r||0===r)&&(l+=r),l+=' ui-icon-shadow">&nbsp;</span></div></li>'}function v(a,i,e){var t="";return t+=" ui-icon-"+g(n.get.call(a,e.icon,{hash:{"default":a},data:i}))}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),t=t||{};var b,m="",g=this.escapeExpression,y="function",D=this,W=n.blockHelperMissing;return b=n.foreach.call(i,i.items,{hash:{},inverse:D.noop,fn:D.programWithDepth(1,r,t,i),data:t}),(b||0===b)&&(m+=b),m})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.loadmask=a(function(a,e,n,r,l){function o(a){var e="";return e+=" loadmask-"+y(typeof a===m?a.apply(a):a)}function s(){var a="";return a+=" loadmask-fullscreen"}function p(){var a="";return a+=" loadmask-no-fullscreen"}function t(a){var e="";return e+=' style="'+y(typeof a===m?a.apply(a):a)+'"'}function i(a){var e="";return e+=' id="'+y(typeof a===m?a.apply(a):a)+'"'}function f(a){var e="";return e+=" loadmask-msg-"+y(typeof a===m?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),l=l||{};var c,d,u="",m="function",y=this.escapeExpression,v=this,h=n.blockHelperMissing;return u+='<div class="loadmask',c=e.ui,c=typeof c===m?c.apply(e):c,d=h.call(e,c,{hash:{},inverse:v.noop,fn:v.program(1,o,l),data:l}),(d||0===d)&&(u+=d),d=n["if"].call(e,e.fullscreen,{hash:{},inverse:v.program(5,p,l),fn:v.program(3,s,l),data:l}),(d||0===d)&&(u+=d),u+='"',c=e.style,c=typeof c===m?c.apply(e):c,d=h.call(e,c,{hash:{},inverse:v.noop,fn:v.program(7,t,l),data:l}),(d||0===d)&&(u+=d),c=e.id,c=typeof c===m?c.apply(e):c,d=h.call(e,c,{hash:{},inverse:v.noop,fn:v.program(9,i,l),data:l}),(d||0===d)&&(u+=d),u+='><div class="loadmask-msg',c=e.ui,c=typeof c===m?c.apply(e):c,d=h.call(e,c,{hash:{},inverse:v.noop,fn:v.program(11,f,l),data:l}),(d||0===d)&&(u+=d),u+='">Loading...</div></div>'})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.page=a(function(a,e,p,t,r){function n(a){var e="";return e+=" ui-body-"+u(typeof a===c?a.apply(a):a)}function s(a){var e="";return e+=" "+u(typeof a===c?a.apply(a):a)}function i(a){var e="";return e+=' style="'+u(typeof a===c?a.apply(a):a)+'"'}function o(a){var e="";return e+=' id="'+u(typeof a===c?a.apply(a):a)+'"'}this.compilerInfo=[4,">= 1.0.0"],p=this.merge(p,a.helpers),t=this.merge(t,a.partials),r=r||{};var l,f,y="",c="function",u=this.escapeExpression,h=this,v=p.blockHelperMissing;return y+='<div class="ui-page ui-page-active',l=e.ui,l=typeof l===c?l.apply(e):l,f=v.call(e,l,{hash:{},inverse:h.noop,fn:h.program(1,n,r),data:r}),(f||0===f)&&(y+=f),l=e.cssClass,l=typeof l===c?l.apply(e):l,f=v.call(e,l,{hash:{},inverse:h.noop,fn:h.program(3,s,r),data:r}),(f||0===f)&&(y+=f),y+='"',l=e.style,l=typeof l===c?l.apply(e):l,f=v.call(e,l,{hash:{},inverse:h.noop,fn:h.program(5,i,r),data:r}),(f||0===f)&&(y+=f),l=e.id,l=typeof l===c?l.apply(e):l,f=v.call(e,l,{hash:{},inverse:h.noop,fn:h.program(7,o,r),data:r}),(f||0===f)&&(y+=f),y+=">",f=h.invokePartial(t.items,"items",e,p,t,r),(f||0===f)&&(y+=f),y+="</div>"})}();!function(){var e=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.panel=e(function(e,a,n,i,r){function l(e){var a="";return a+=" ui-body-"+d(typeof e===v?e.apply(e):e)}function p(e){var a="";return a+=' id="'+d(typeof e===v?e.apply(e):e)+'-overlay"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,e.helpers),i=this.merge(i,e.partials),r=r||{};var t,s,o="",v="function",d=this.escapeExpression,u=this,c=n.blockHelperMissing;return o+='<div class="ui-panel',t=a.ui,t=typeof t===v?t.apply(a):t,s=c.call(a,t,{hash:{},inverse:u.noop,fn:u.program(1,l,r),data:r}),(s||0===s)&&(o+=s),o+=' panel-menu overthrow"',s=u.invokePartial(i.id,"id",a,n,i,r),(s||0===s)&&(o+=s),o+='><div class="ui-panel-inner panel-menu-inner">',s=u.invokePartial(i.items,"items",a,n,i,r),(s||0===s)&&(o+=s),o+='</div></div><div class="panel-menu-overlay"',t=a.id,t=typeof t===v?t.apply(a):t,s=c.call(a,t,{hash:{},inverse:u.noop,fn:u.program(3,p,r),data:r}),(s||0===s)&&(o+=s),o+=">&nbsp;</div>"})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.popup=a(function(a,n,e,r,o){function p(){return" hidden"}function t(a){var n,e="";return e+=" floatingpanel-overlay-"+P((n=a.overlay,typeof n===D?n.apply(a):n))}function i(){return" floatingpanel-overlay-default"}function l(a){var n="";return n+=' id="'+P(typeof a===D?a.apply(a):a)+'-overlay"'}function s(){return" ui-overlay-shadow"}function f(a){var n="";return n+=" ui-corner-"+P(typeof a===D?a.apply(a):a)}function u(a){var n="";return n+="margin-"+P(typeof a===D?a.apply(a):a)+": 0px;"}function c(a){return P(typeof a===D?a.apply(a):a)}function h(a,n,r){var o,p,t="";return t+='<div class="ui-header',o=r.ui,o=typeof o===D?o.apply(a):o,p=_.call(a,o,{hash:{},inverse:W.noop,fn:W.program(18,y,n),data:n}),(p||0===p)&&(t+=p),t+='">',o=a.header,o=null==o||o===!1?o:o.title,o=typeof o===D?o.apply(a):o,p=_.call(a,o,{hash:{},inverse:W.noop,fn:W.program(20,d,n),data:n}),(p||0===p)&&(t+=p),p=e["if"].call(a,(o=a.header,null==o||o===!1?o:o.close),{hash:{},inverse:W.noop,fn:W.programWithDepth(22,v,n,r),data:n}),(p||0===p)&&(t+=p),t+="</div>"}function y(a){var n="";return n+=" ui-bar-"+P(typeof a===D?a.apply(a):a)}function d(a){var n="";return n+='<h1 class="ui-title">'+P(typeof a===D?a.apply(a):a)+"</h1>"}function v(a,n,e){var r,o,p="";return p+='<a href="javascript:;" class="ui-btn-'+P((r=a.header,r=null==r||r===!1?r:r.close,typeof r===D?r.apply(a):r))+" ui-btn",r=e.shadow,r=typeof r===D?r.apply(a):r,o=_.call(a,r,{hash:{},inverse:W.noop,fn:W.program(23,m,n),data:n}),(o||0===o)&&(p+=o),r=e.corner,r=typeof r===D?r.apply(a):r,o=_.call(a,r,{hash:{},inverse:W.noop,fn:W.program(25,g,n),data:n}),(o||0===o)&&(p+=o),p+=" ui-btn-icon-notext",r=e.ui,r=typeof r===D?r.apply(a):r,o=_.call(a,r,{hash:{},inverse:W.noop,fn:W.program(27,b,n),data:n}),(o||0===o)&&(p+=o),p+='"',r=e.id,r=typeof r===D?r.apply(a):r,o=_.call(a,r,{hash:{},inverse:W.noop,fn:W.program(29,w,n),data:n}),(o||0===o)&&(p+=o),p+='><span class="ui-btn-inner"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a>'}function m(){return" ui-shadow"}function g(a){var n="";return n+=" ui-btn-corner-"+P(typeof a===D?a.apply(a):a)}function b(a){var n="";return n+=" ui-btn-up-"+P(typeof a===D?a.apply(a):a)}function w(a){var n="";return n+=' id="'+P(typeof a===D?a.apply(a):a)+'-close"'}function x(a){var n="";return n+=" ui-body-"+P(typeof a===D?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,a.helpers),r=this.merge(r,a.partials),o=o||{};var H,k,q="",D="function",P=this.escapeExpression,W=this,_=e.blockHelperMissing;return q+='<div class="floatingpanel-overlay',H=e.if_eq.call(n,n.hidden,{hash:{compare:!0,"default":!0},inverse:W.noop,fn:W.program(1,p,o),data:o}),(H||0===H)&&(q+=H),H=e["if"].call(n,n.overlay,{hash:{},inverse:W.program(5,i,o),fn:W.program(3,t,o),data:o}),(H||0===H)&&(q+=H),q+='"',H=n.id,H=typeof H===D?H.apply(n):H,k=_.call(n,H,{hash:{},inverse:W.noop,fn:W.program(7,l,o),data:o}),(k||0===k)&&(q+=k),q+='></div><div class="ui-dialog-contain',H=n.shadow,H=typeof H===D?H.apply(n):H,k=_.call(n,H,{hash:{},inverse:W.noop,fn:W.program(9,s,o),data:o}),(k||0===k)&&(q+=k),H=n.corner,H=typeof H===D?H.apply(n):H,k=_.call(n,H,{hash:{},inverse:W.noop,fn:W.program(11,f,o),data:o}),(k||0===k)&&(q+=k),k=e.if_eq.call(n,n.hidden,{hash:{compare:!0,"default":!0},inverse:W.noop,fn:W.program(1,p,o),data:o}),(k||0===k)&&(q+=k),q+='" style="z-index: 10000;',H=n.actionSheet,H=typeof H===D?H.apply(n):H,k=_.call(n,H,{hash:{},inverse:W.noop,fn:W.program(13,u,o),data:o}),(k||0===k)&&(q+=k),H=n.style,H=typeof H===D?H.apply(n):H,k=_.call(n,H,{hash:{},inverse:W.noop,fn:W.program(15,c,o),data:o}),(k||0===k)&&(q+=k),q+='"',k=W.invokePartial(r.id,"id",n,e,r,o),(k||0===k)&&(q+=k),q+=">",k=e["if"].call(n,n.header,{hash:{},inverse:W.noop,fn:W.programWithDepth(17,h,o,n),data:o}),(k||0===k)&&(q+=k),q+='<div class="ui-content',H=n.ui,H=typeof H===D?H.apply(n):H,k=_.call(n,H,{hash:{},inverse:W.noop,fn:W.program(31,x,o),data:o}),(k||0===k)&&(q+=k),q+='">',k=W.invokePartial(r.items,"items",n,e,r,o),(k||0===k)&&(q+=k),q+="</div></div>"})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.radiobutton=a(function(a,n,e,r,o){function p(){return"on"}function t(){return"off"}function i(a){var n="";return n+=" ui-btn-corner-"+k(typeof a===b?a.apply(a):a)}function l(){return" ui-shadow"}function s(){return" ui-mini"}function f(a){var n="";return n+=" ui-btn-up-"+k(typeof a===b?a.apply(a):a)}function u(a){var n="";return n+=' id="'+k(typeof a===b?a.apply(a):a)+'-ui"'}function c(a){return k(typeof a===b?a.apply(a):a)}function d(){return" ui-icon-shadow"}function h(a){var n="";return n+=' name="'+k(typeof a===b?a.apply(a):a)+'"'}function y(a){var n="";return n+=' value="'+k(typeof a===b?a.apply(a):a)+'"'}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,a.helpers),r=this.merge(r,a.partials),o=o||{};var m,v,g="",b="function",k=this.escapeExpression,_=this,w=e.blockHelperMissing;return g+='<div class="ui-radio"><label class="ui-radio-',m=e.if_eq.call(n,n.checked,{hash:{compare:!0,"default":!1},inverse:_.program(3,t,o),fn:_.program(1,p,o),data:o}),(m||0===m)&&(g+=m),g+=" ui-btn",m=n.corner,m=typeof m===b?m.apply(n):m,v=w.call(n,m,{hash:{},inverse:_.noop,fn:_.program(5,i,o),data:o}),(v||0===v)&&(g+=v),m=n.shadow,m=typeof m===b?m.apply(n):m,v=w.call(n,m,{hash:{},inverse:_.noop,fn:_.program(7,l,o),data:o}),(v||0===v)&&(g+=v),v=e.if_eq.call(n,n.size,{hash:{compare:"small","default":"medium"},inverse:_.noop,fn:_.program(9,s,o),data:o}),(v||0===v)&&(g+=v),g+=" ui-fullsize ui-btn-icon-"+k(e.get.call(n,n.iconPos,{hash:{"default":"left",choices:"left|right"},data:o})),m=n.ui,m=typeof m===b?m.apply(n):m,v=w.call(n,m,{hash:{},inverse:_.noop,fn:_.program(11,f,o),data:o}),(v||0===v)&&(g+=v),g+='"',v=_.invokePartial(r.for_id,"for_id",n,e,r,o),(v||0===v)&&(g+=v),m=n.id,m=typeof m===b?m.apply(n):m,v=w.call(n,m,{hash:{},inverse:_.noop,fn:_.program(13,u,o),data:o}),(v||0===v)&&(g+=v),g+='><span class="ui-btn-inner"><span class="ui-btn-text">',m=n.label,m=typeof m===b?m.apply(n):m,v=w.call(n,m,{hash:{},inverse:_.noop,fn:_.program(15,c,o),data:o}),(v||0===v)&&(g+=v),g+='</span><span class="ui-icon ui-icon-radio-',v=e.if_eq.call(n,n.checked,{hash:{compare:!0,"default":!1},inverse:_.program(3,t,o),fn:_.program(1,p,o),data:o}),(v||0===v)&&(g+=v),m=n.shadow,m=typeof m===b?m.apply(n):m,v=w.call(n,m,{hash:{},inverse:_.noop,fn:_.program(17,d,o),data:o}),(v||0===v)&&(g+=v),g+='">&nbsp;</span></span></label><input type="radio"',m=n.name,m=typeof m===b?m.apply(n):m,v=w.call(n,m,{hash:{},inverse:_.noop,fn:_.program(19,h,o),data:o}),(v||0===v)&&(g+=v),m=n.value,m=typeof m===b?m.apply(n):m,v=w.call(n,m,{hash:{},inverse:_.noop,fn:_.program(21,y,o),data:o}),(v||0===v)&&(g+=v),v=_.invokePartial(r.id,"id",n,e,r,o),(v||0===v)&&(g+=v),g+="></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.select=a(function(a,e,n,r,t){function o(a,e){var t,o="";return o+='<label class="ui-select"',t=H.invokePartial(r.for_id,"for_id",a,n,r,e),(t||0===t)&&(o+=t),o+=">"+x(typeof a===k?a.apply(a):a)+"</label>"}function p(a){var e="";return e+=' id="'+x(typeof a===k?a.apply(a):a)+'-ui"'}function i(){return" ui-shadow"}function l(a){var e="";return e+=" ui-btn-corner-"+x(typeof a===k?a.apply(a):a)}function s(){return" ui-disabled"}function c(){return" ui-mini"}function u(a){var e="";return e+=" ui-btn-up-"+x(typeof a===k?a.apply(a):a)}function f(a,e,r){var t="";return t+=x(n.getLabelFromValue.call(a,a,r.options,{hash:{},data:e}))}function d(a,e){var n,r,t="";return n=a.emptyText,n=typeof n===k?n.apply(a):n,r=P.call(a,n,{hash:{},inverse:H.noop,fn:H.program(18,h,e),data:e}),(r||0===r)&&(t+=r),t}function h(a){return x(typeof a===k?a.apply(a):a)}function v(){return" ui-icon-shadow"}function m(a){var e="";return e+=' name="'+x(typeof a===k?a.apply(a):a)+'"'}function y(a,e,r){var t,o,p="";return p+='<option value="'+x((t=a.value,typeof t===k?t.apply(a):t))+'"',o=n.if_eq.call(a,a.value,{hash:{compare:r.value},inverse:H.noop,fn:H.program(25,g,e),data:e}),(o||0===o)&&(p+=o),o=n.if_eq.call(a,a.disabled,{hash:{compare:!0,"default":!1},inverse:H.noop,fn:H.program(27,b,e),data:e}),(o||0===o)&&(p+=o),p+=">"+x((t=a.label,typeof t===k?t.apply(a):t))+"</option>"}function g(){return' selected="selected"'}function b(){return' disabled="disabled"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),r=this.merge(r,a.partials),t=t||{};var _,w,q="",H=this,k="function",x=this.escapeExpression,P=n.blockHelperMissing;return _=e.label,_=typeof _===k?_.apply(e):_,w=P.call(e,_,{hash:{},inverse:H.noop,fn:H.program(1,o,t),data:t}),(w||0===w)&&(q+=w),q+="<div",_=e.id,_=typeof _===k?_.apply(e):_,w=P.call(e,_,{hash:{},inverse:H.noop,fn:H.program(3,p,t),data:t}),(w||0===w)&&(q+=w),q+=' class="ui-select"><div class="ui-btn',_=e.shadow,_=typeof _===k?_.apply(e):_,w=P.call(e,_,{hash:{},inverse:H.noop,fn:H.program(5,i,t),data:t}),(w||0===w)&&(q+=w),_=e.corner,_=typeof _===k?_.apply(e):_,w=P.call(e,_,{hash:{},inverse:H.noop,fn:H.program(7,l,t),data:t}),(w||0===w)&&(q+=w),w=n.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:H.noop,fn:H.program(9,s,t),data:t}),(w||0===w)&&(q+=w),w=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:H.noop,fn:H.program(11,c,t),data:t}),(w||0===w)&&(q+=w),q+=" ui-btn-icon-"+x(n.get.call(e,e.iconPos,{hash:{"default":"right",choices:"left|right"},data:t})),_=e.ui,_=typeof _===k?_.apply(e):_,w=P.call(e,_,{hash:{},inverse:H.noop,fn:H.program(13,u,t),data:t}),(w||0===w)&&(q+=w),q+='"><span class="ui-btn-inner"><span class="ui-btn-text">',w=n["if"].call(e,e.value,{hash:{},inverse:H.program(17,d,t),fn:H.programWithDepth(15,f,t,e),data:t}),(w||0===w)&&(q+=w),q+='</span><span class="ui-icon ui-icon-arrow-d',_=e.shadow,_=typeof _===k?_.apply(e):_,w=P.call(e,_,{hash:{},inverse:H.noop,fn:H.program(20,v,t),data:t}),(w||0===w)&&(q+=w),q+='">&nbsp;</span><select',_=e.name,_=typeof _===k?_.apply(e):_,w=P.call(e,_,{hash:{},inverse:H.noop,fn:H.program(22,m,t),data:t}),(w||0===w)&&(q+=w),w=H.invokePartial(r.id,"id",e,n,r,t),(w||0===w)&&(q+=w),q+=">",w=n.each.call(e,e.options,{hash:{},inverse:H.noop,fn:H.programWithDepth(24,y,t,e),data:t}),(w||0===w)&&(q+=w),q+="</select></span></div></div>"})}();!function(){var e=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.selectitembasic=e(function(e,a,t,n,l){function r(e,a,n){var l,r,i="";return i+='<option value="'+d((l=e.value,typeof l===c?l.apply(e):l))+'"',r=t.if_eq.call(e,e.value,{hash:{compare:n.value},inverse:f.noop,fn:f.program(2,o,a),data:a}),(r||0===r)&&(i+=r),r=t.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:f.noop,fn:f.program(4,s,a),data:a}),(r||0===r)&&(i+=r),i+=">"+d((l=e.label,typeof l===c?l.apply(e):l))+"</option>"}function o(){return' selected="selected"'}function s(){return' disabled="disabled"'}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,e.helpers),l=l||{};var i,p="",c="function",d=this.escapeExpression,f=this;return i=t.each.call(a,a.options,{hash:{},inverse:f.noop,fn:f.programWithDepth(1,r,l,a),data:l}),(i||0===i)&&(p+=i),p})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.tabbutton=a(function(a,n,t,e,o){function p(a,n,e){var o="";return o+=" ui-btn-icon-"+y(t.get.call(a,e.iconPos,{hash:{"default":"top",choices:"top|bottom|left|right"},data:n}))}function r(a){var n="";return n+=" ui-btn-up-"+y(typeof a===v?a.apply(a):a)}function i(a){var n="";return n+=' style="'+y(typeof a===v?a.apply(a):a)+'"'}function s(a){var n="";return n+='<span class="ui-btn-text">'+y(typeof a===v?a.apply(a):a)+"</span>"}function l(a,n){var e,o="";return o+='<span class="icon override-tab-icon">',e=t.geticon.call(a,a,{hash:{},inverse:b.noop,fn:b.program(10,c,n),data:n}),(e||0===e)&&(o+=e),o+="</span>"}function c(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,a.helpers),e=this.merge(e,a.partials),o=o||{};var f,u,h="",y=this.escapeExpression,v="function",b=this,d=t.blockHelperMissing;return h+='<a class="ui-btn ui-btn-inline',f=n.icon,f=typeof f===v?f.apply(n):f,u=d.call(n,f,{hash:{},inverse:b.noop,fn:b.programWithDepth(1,p,o,n),data:o}),(u||0===u)&&(h+=u),f=n.ui,f=typeof f===v?f.apply(n):f,u=d.call(n,f,{hash:{},inverse:b.noop,fn:b.program(3,r,o),data:o}),(u||0===u)&&(h+=u),h+='"',f=n.style,f=typeof f===v?f.apply(n):f,u=d.call(n,f,{hash:{},inverse:b.noop,fn:b.program(5,i,o),data:o}),(u||0===u)&&(h+=u),u=b.invokePartial(e.id,"id",n,t,e,o),(u||0===u)&&(h+=u),h+='><span class="ui-btn-inner">',f=n.text,f=typeof f===v?f.apply(n):f,u=d.call(n,f,{hash:{},inverse:b.noop,fn:b.program(7,s,o),data:o}),(u||0===u)&&(h+=u),f=n.icon,f=typeof f===v?f.apply(n):f,u=d.call(n,f,{hash:{},inverse:b.noop,fn:b.program(9,l,o),data:o}),(u||0===u)&&(h+=u),h+="</span></a>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.tabs=a(function(a,e,s,t,n){function i(){return" ui-mini"}function r(a){var e="";return e+=' id="'+d(typeof a===u?a.apply(a):a)+'"'}function l(a,e){var t,n,i="";return i+='<li class="ui-block-'+d(s.getIndexLetter.call(a,(t=e,null==t||t===!1?t:t.index),{hash:{},data:e}))+'">',n=s.safe.call(a,a,{hash:{},inverse:f.noop,fn:f.program(6,o,e),data:e}),(n||0===n)&&(i+=n),i+="</li>"}function o(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],s=this.merge(s,a.helpers),t=this.merge(t,a.partials),n=n||{};var c,p,h="",u="function",d=this.escapeExpression,f=this,m=s.blockHelperMissing;return h+='<div class="ui-navbar',c=s.if_eq.call(e,e.mini,{hash:{compare:!0,"default":!0},inverse:f.noop,fn:f.program(1,i,n),data:n}),(c||0===c)&&(h+=c),c=f.invokePartial(t.cssClass,"cssClass",e,s,t,n),(c||0===c)&&(h+=c),h+='"',c=f.invokePartial(t.style,"style",e,s,t,n),(c||0===c)&&(h+=c),c=e.id,c=typeof c===u?c.apply(e):c,p=m.call(e,c,{hash:{},inverse:f.noop,fn:f.program(3,r,n),data:n}),(p||0===p)&&(h+=p),h+='><ul class="ui-grid-'+d(s.getLengthLetter.call(e,e.items,{hash:{},data:n}))+'">',p=s.each.call(e,e.items,{hash:{},inverse:f.noop,fn:f.program(5,l,n),data:n}),(p||0===p)&&(h+=p),h+="</ul></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.textarea=a(function(a,e,r,n,t){function o(a,e){var t,o="";return o+='<label class="ui-input-text"',t=x.invokePartial(n.for_id,"for_id",a,r,n,e),(t||0===t)&&(o+=t),o+=">"+H(typeof a===q?a.apply(a):a)+"</label>"}function p(a){var e="";return e+=" ui-body-"+H(typeof a===q?a.apply(a):a)}function l(){return" ui-disabled"}function i(a){var e="";return e+=' name="'+H(typeof a===q?a.apply(a):a)+'"'}function s(a){var e="";return e+=' placeholder="'+H(typeof a===q?a.apply(a):a)+'"'}function f(){return' disabled="disabled"'}function u(){return" required"}function c(a){var e;return H((e=a.cols,typeof e===q?e.apply(a):e))}function d(){return"40"}function h(a){var e;return H((e=a.rows,typeof e===q?e.apply(a):e))}function y(){return"8"}function m(a){return H(typeof a===q?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],r=this.merge(r,a.helpers),n=this.merge(n,a.partials),t=t||{};var v,g,b="",x=this,q="function",H=this.escapeExpression,_=r.blockHelperMissing;return v=e.label,v=typeof v===q?v.apply(e):v,g=_.call(e,v,{hash:{},inverse:x.noop,fn:x.program(1,o,t),data:t}),(g||0===g)&&(b+=g),b+='<textarea class="ui-input-text ui-shadow-inset ui-corner-all ',v=e.ui,v=typeof v===q?v.apply(e):v,g=_.call(e,v,{hash:{},inverse:x.noop,fn:x.program(3,p,t),data:t}),(g||0===g)&&(b+=g),g=r.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:x.noop,fn:x.program(5,l,t),data:t}),(g||0===g)&&(b+=g),b+='"',v=e.name,v=typeof v===q?v.apply(e):v,g=_.call(e,v,{hash:{},inverse:x.noop,fn:x.program(7,i,t),data:t}),(g||0===g)&&(b+=g),v=e.placeHolder,v=typeof v===q?v.apply(e):v,g=_.call(e,v,{hash:{},inverse:x.noop,fn:x.program(9,s,t),data:t}),(g||0===g)&&(b+=g),g=r.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:x.noop,fn:x.program(11,f,t),data:t}),(g||0===g)&&(b+=g),g=r.if_eq.call(e,e.required,{hash:{compare:!0,"default":!1},inverse:x.noop,fn:x.program(13,u,t),data:t}),(g||0===g)&&(b+=g),b+=' cols="',g=r["if"].call(e,e.cols,{hash:{},inverse:x.program(17,d,t),fn:x.program(15,c,t),data:t}),(g||0===g)&&(b+=g),b+='" rows="',g=r["if"].call(e,e.rows,{hash:{},inverse:x.program(21,y,t),fn:x.program(19,h,t),data:t}),(g||0===g)&&(b+=g),b+='"',g=x.invokePartial(n.id,"id",e,r,n,t),(g||0===g)&&(b+=g),b+=">",v=e.value,v=typeof v===q?v.apply(e):v,g=_.call(e,v,{hash:{},inverse:x.noop,fn:x.program(23,m,t),data:t}),(g||0===g)&&(b+=g),b+="</textarea>"})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.textfield=a(function(a,n,e,r,t){function o(){return'<div class="ui-field-contain ui-body ui-br">'}function p(a,n){var t,o="";return o+='<label class="ui-input-text"',t=F.invokePartial(r.for_id,"for_id",a,e,r,n),(t||0===t)&&(o+=t),o+=">"+J(typeof a===G?a.apply(a):a)+"</label>"}function i(){return"ui-input-search"}function l(){return"ui-input-text"}function u(){return" ui-shadow-inset"}function s(){return"btn-"}function f(a){var n="";return n+=" ui-body-"+J(typeof a===G?a.apply(a):a)}function c(a){var n="";return n+=" "+J(typeof a===G?a.apply(a):a)}function d(a){var n="";return n+=" ui-icon-"+J(typeof a===G?a.apply(a):a)+"field"}function h(){return" ui-mini"}function y(){return" ui-disabled"}function v(a){var n;return J((n=a.type,typeof n===G?n.apply(a):n))}function m(){return"text"}function g(a){var n="";return n+=' autocomplete="'+J(typeof a===G?a.apply(a):a)+'"'}function b(a){var n="";return n+=' name="'+J(typeof a===G?a.apply(a):a)+'"'}function q(a){var n="";return n+=' pattern="'+J(typeof a===G?a.apply(a):a)+'"'}function x(a){var n="";return n+=' value="'+J(typeof a===G?a.apply(a):a)+'"'}function _(a){var n="";return n+=' placeholder="'+J(typeof a===G?a.apply(a):a)+'"'}function w(){var a="";return a+=" mobile-textinput-disabled ui-state-disabled"}function H(){return' disabled="disabled"'}function k(){return" required"}function I(){return" readonly"}function z(a,n){var r,t,o="";return o+='<a href="javascript:;" class="ui-input-clear ui-btn',r=a.ui,r=typeof r===G?r.apply(a):r,t=K.call(a,r,{hash:{},inverse:F.noop,fn:F.program(46,B,n),data:n}),(t||0===t)&&(o+=t),o+=" ui-shadow ui-btn-corner-all ui-fullsize ui-btn-icon-notext",t=e["if"].call(a,a.value,{hash:{},inverse:F.program(49,j,n),fn:F.program(48,P,n),data:n}),(t||0===t)&&(o+=t),o+='"',r=a.id,r=typeof r===G?r.apply(a):r,t=K.call(a,r,{hash:{},inverse:F.noop,fn:F.program(51,C,n),data:n}),(t||0===t)&&(o+=t),o+='><span class="ui-btn-inner"><span class="ui-btn-text">clear text</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a>'}function B(a){var n="";return n+=" ui-btn-up-"+J(typeof a===G?a.apply(a):a)}function P(a,n){var r,t,o="";return t=e.if_gteq.call(a,a.clearBtn,{hash:{compare:(r=a.value,null==r||r===!1?r:r.length),"default":1},inverse:F.noop,fn:F.program(49,j,n),data:n}),(t||0===t)&&(o+=t),o}function j(){var a="";return a+=" ui-input-clear-hidden"}function C(a){var n="";return n+=' id="'+J(typeof a===G?a.apply(a):a)+'-clearbtn"'}function E(){return"</div>"}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,a.helpers),r=this.merge(r,a.partials),t=t||{};var M,A,D="",F=this,G="function",J=this.escapeExpression,K=e.blockHelperMissing;return M=n.labelInline,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(1,o,t),data:t}),(A||0===A)&&(D+=A),M=n.label,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(3,p,t),data:t}),(A||0===A)&&(D+=A),D+='<div class="',A=e["if"].call(n,n.icon,{hash:{},inverse:F.program(7,l,t),fn:F.program(5,i,t),data:t}),(A||0===A)&&(D+=A),M=n.shadow,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(9,u,t),data:t}),(A||0===A)&&(D+=A),D+=" ui-",M=n.icon,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(11,s,t),data:t}),(A||0===A)&&(D+=A),D+="corner-all ui-btn-shadow",M=n.ui,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(13,f,t),data:t}),(A||0===A)&&(D+=A),M=n.cssClass,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(15,c,t),data:t}),(A||0===A)&&(D+=A),M=n.icon,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(17,d,t),data:t}),(A||0===A)&&(D+=A),A=e.if_eq.call(n,n.size,{hash:{compare:"small","default":"medium"},inverse:F.noop,fn:F.program(19,h,t),data:t}),(A||0===A)&&(D+=A),A=e.if_eq.call(n,n.disabled,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(21,y,t),data:t}),(A||0===A)&&(D+=A),D+='"><input type="',A=e["if"].call(n,n.type,{hash:{},inverse:F.program(25,m,t),fn:F.program(23,v,t),data:t}),(A||0===A)&&(D+=A),D+='"',M=n.autocomplete,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(27,g,t),data:t}),(A||0===A)&&(D+=A),M=n.name,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(29,b,t),data:t}),(A||0===A)&&(D+=A),M=n.pattern,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(31,q,t),data:t}),(A||0===A)&&(D+=A),M=n.value,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(33,x,t),data:t}),(A||0===A)&&(D+=A),M=n.placeHolder,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(35,_,t),data:t}),(A||0===A)&&(D+=A),D+='class="ui-input-text',A=e.if_eq.call(n,n.disabled,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(37,w,t),data:t}),(A||0===A)&&(D+=A),M=n.ui,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(13,f,t),data:t}),(A||0===A)&&(D+=A),D+='"',A=e.if_eq.call(n,n.disabled,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(39,H,t),data:t}),(A||0===A)&&(D+=A),A=e.if_eq.call(n,n.required,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(41,k,t),data:t}),(A||0===A)&&(D+=A),A=e.if_eq.call(n,n.readonly,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(43,I,t),data:t}),(A||0===A)&&(D+=A),A=F.invokePartial(r.id,"id",n,e,r,t),(A||0===A)&&(D+=A),D+=">",A=e.if_neq.call(n,n.clearBtn,{hash:{compare:!1,"default":!1},inverse:F.noop,fn:F.program(45,z,t),data:t}),(A||0===A)&&(D+=A),D+="</div>",M=n.labelInline,M=typeof M===G?M.apply(n):M,A=K.call(n,M,{hash:{},inverse:F.noop,fn:F.program(53,E,t),data:t}),(A||0===A)&&(D+=A),D})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.toolbar=a(function(a,e,s,t,i){function r(a){var e="";return e+="ui-bar-"+c(typeof a===p?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],s=this.merge(s,a.helpers),t=this.merge(t,a.partials),i=i||{};var l,n,o="",p="function",c=this.escapeExpression,m=this,f=s.blockHelperMissing;return o+='<div class="',l=e.ui,l=typeof l===p?l.apply(e):l,n=f.call(e,l,{hash:{},inverse:m.noop,fn:m.program(1,r,i),data:i}),(n||0===n)&&(o+=n),n=m.invokePartial(t.cssClass,"cssClass",e,s,t,i),(n||0===n)&&(o+=n),o+='">',n=m.invokePartial(t.items,"items",e,s,t,i),(n||0===n)&&(o+=n),o+="</div>"})}();


Fs.engines.JqueryMobile = (function() {

    var _handlebars = Handlebars,
        _fs = Fs,
        _fsevent = _fs.events;
        _parent = _fs,
        _selector = _fs.Selector,
        _templates = _fs.Templates,
        _tpls = _handlebars.templates,
        _events = {},
        _overrides = {};

    
    _handlebars.registerHelper('getIndexLetter', function(index) {
        return String.fromCharCode(97 + (index % 26));
    });

    _handlebars.registerHelper('getLengthLetter', function(array) {
        var length = array.length;

        if (length < 2) {
            return 'a';
        }
        return String.fromCharCode(95 + length);
    });

    
    function _removeUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            _selector.removeClass(el, pattern.replace('{{ui}}', ui[len]));
        }
    };

    function _addUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            _selector.addClass(el, pattern.replace('{{ui}}', ui[len]));
        }
    };

    function updateMarginTop() {
        if (this.el) {
            return;
        }
        var el = this.el,
            height = el.offsetHeight / 2;

        el.style.marginTop = '-' + height + 'px';
    };

    _events.alert = {
        show: function(cmp) {
            if (this.el) {
                updateMarginTop.call(this);
            } else {
                setTimeout(updateMarginTop.bind(this), 150);
            }
        }
    };

    _events.collapsible = {
        afterrender: function() {
            this.iconEl = _selector.get('.ui-icon', this.el);
            this.iconCollapsed = 'ui-icon-' + (this.config.iconCollapsed || 'plus');
            this.iconExpanded = 'ui-icon-' + (this.config.iconExpanded || 'minus');
        },
        collapse: function() {
            _selector.addClass(this.headerEl, 'ui-collapsible-heading-collapsed');
            _selector.addClass(this.contentEl, 'ui-collapsible-content-collapsed');
            _selector.addClass(this.el, 'ui-collapsible-collapsed');
            
            _selector.removeClass(this.iconEl, this.iconExpanded);
            _selector.addClass(this.iconEl, this.iconCollapsed);
        },
        expand: function() {
            _selector.removeClass(this.headerEl, 'ui-collapsible-heading-collapsed');
            _selector.removeClass(this.contentEl, 'ui-collapsible-content-collapsed');
            _selector.removeClass(this.el, 'ui-collapsible-collapsed');
            
            _selector.removeClass(this.iconEl, this.iconCollapsed);
            _selector.addClass(this.iconEl, this.iconExpanded);
        }
    };

    _handlebars.registerHelper('getLabelFromValue', function(value, values) {
        var item,
            i = values.length;

        while (i--) {
            item = values[i];
            if (item.value === value) {
                return item.label;
            }
        }
        return value;
    });
    _events.select = {
        change: function(cmp, item) {
            var textEl = _selector.get('.ui-btn-text', this.uiEl);

            if (textEl) {
                textEl.innerHTML = item.label;
            }
        },
        loading: function(cmp, isLoading) {
            var textEl = _selector.get('.ui-btn-text', this.uiEl);

            if (isLoading) {
                cmp.setDisabled(true);
                if (cmp.config.emptyText) {
                    textEl.innerHTML = 'Loading';
                }
            } else {
                cmp.setDisabled(false);
                if (cmp.config.emptyText) {
                    textEl.innerHTML = cmp.config.emptyText;
                }
            }
        },
        disabled: function(cmp, isDisabled) {
            var btnEl = _selector.get('.ui-btn', this.uiEl);

            if (isDisabled) {
                _selector.addClass(btnEl, 'ui-disabled');
            } else {
                _selector.removeClass(btnEl, 'ui-disabled');
            }
        }
    };

    _events.button = {
        disabled: function(cmp, isDisabled) {
            if (isDisabled) {
                _selector.addClass(this.el, 'ui-disabled');
            } else {
                _selector.removeClass(this.el, 'ui-disabled');
            }
        }
    };

    function _setUIBtn(ui) {
        _removeUI('ui-btn-up-{{ui}}', this.config.ui, this.el);
        _addUI('ui-btn-up-{{ui}}', ui, this.el);

        this.config.ui = ui;
    };

    _fs.views.Button.prototype.setUI = _setUIBtn;
    _fs.views.TabButton.prototype.setUI = _setUIBtn;

    function _setUIBar(ui) {
        _removeUI('ui-bar-{{ui}}', this.config.ui, this.el);
        _addUI('ui-bar-{{ui}}', ui, this.el);

        this.config.ui = ui;
    };

    _fs.views.Header.prototype.setUI = _setUIBar;
    _fs.views.Footer.prototype.setUI = _setUIBar;
    _fs.views.Toolbar.prototype.setUI = _setUIBar;

    _events.radiobutton = {
        check: function() {
            if (!this.iconEl) {
                this.iconEl = _selector.get('.ui-icon', this.uiEl);
            }
            _selector.removeClass(this.iconEl, 'ui-icon-radio-off');
            _selector.addClass(this.iconEl, 'ui-icon-radio-on');
            _selector.removeClass(this.uiEl, 'ui-radio-off');
            _selector.addClass(this.uiEl, 'ui-radio-on');
        },
        uncheck: function() {
            if (!this.iconEl) {
                this.iconEl = _selector.get('.ui-icon', this.uiEl);
            }
            _selector.removeClass(this.iconEl, 'ui-icon-radio-on');
            _selector.addClass(this.iconEl, 'ui-icon-radio-off');
            _selector.removeClass(this.uiEl, 'ui-radio-on');
            _selector.addClass(this.uiEl, 'ui-radio-off');
        }
    };

    _events.panel = {
        afterrender: function() {
            overthrow.set();
        },
        expand: function() {
            var page = _selector.get('.ui-page-active');

            _selector.addClass(page, 'active');
            _selector.addClass(this.el, 'active');
        },
        collapse: function() {
            var page = _selector.get('.ui-page-active');

            _selector.removeClass(page, 'active');
            _selector.removeClass(this.el, 'active');
        }
    };

    _handlebars.registerHelper('isFlipSwitchOptionActive', function(values, index, value, options) {
        value = value || values[0].value;
        var current = values[index].value;

        if (current === value) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    _events.flipswitch = {
        afterrender: function() {
            var on = _selector.get('.ui-btn-active', this.uiEl),
                off = _selector.get('.ui-btn-down-c', this.uiEl),
                handle = _selector.get('.ui-slider-handle', this.uiEl);
            this.on('check', function() {
                on.style.width = '100%';
                off.style.width = '0%';
                handle.style.left = '100%';
            }, this, this.priority.VIEWS);
            this.on('uncheck', function() {
                on.style.width = '0%';
                off.style.width = '100%';
                handle.style.left = '0%';
            }, this, this.priority.VIEWS);
        }
    };

    _events.textarea = {
        afterrender: function() {
            this.eventFocus = new _fsevent.Focus({
                autoAttach: true,
                el: this.el,
                scope: this,
                handler: function() {
                    _selector.addClass(this.el, 'ui-focus');
                }
            });
            this.eventBlur = new _fsevent.Blur({
                autoAttach: true,
                el: this.el,
                scope: this,
                handler: function() {
                    _selector.removeClass(this.el, 'ui-focus');
                }
            });
        }
    };

    _events.checkbox = {
        afterrender: function() {
            var iconEl = _selector.get('.ui-icon', this.uiEl);
            this.on('check', function() {
                _selector.removeClass(this.uiEl, 'ui-checkbox-off');
                _selector.removeClass(iconEl, 'ui-icon-checkbox-off');
                _selector.addClass(this.uiEl, 'ui-checkbox-on');
                _selector.addClass(iconEl, 'ui-icon-checkbox-on');
            }, this, this.priority.VIEWS);
            this.on('uncheck', function() {
                _selector.removeClass(this.uiEl, 'ui-checkbox-on');
                _selector.removeClass(iconEl, 'ui-icon-checkbox-on');
                _selector.addClass(this.uiEl, 'ui-checkbox-off');
                _selector.addClass(iconEl, 'ui-icon-checkbox-off');
            }, this, this.priority.VIEWS);
        }
    };

    _events.textfield = {
        afterrender: function() {
            var parentNode = this.el.parentNode,
                self = this;

            self.eventFocus = new _fsevent.Focus({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function() {
                    _selector.addClass(parentNode, 'ui-focus');
                }
            });
            self.eventBlur = new _fsevent.Blur({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function() {
                    _selector.removeClass(parentNode, 'ui-focus');
                }
            });
            if (self.config.clearBtn) {
                self.clearBtnEl = document.getElementById(self.config.id + '-clearbtn');
                var keyupHandler = function() {
                    if (self.config.clearBtn <= self.el.value.length) {
                        _selector.removeClass(self.clearBtnEl, 'ui-input-clear-hidden');
                    } else {
                        _selector.addClass(self.clearBtnEl, 'ui-input-clear-hidden');
                    }
                };
                self.eventKeyup = new _fsevent.Abstract({
                    eventName: 'keyup',
                    autoAttach: true,
                    el: self.el,
                    globalFwd: window,
                    scope: self,
                    handler: keyupHandler
                });
                self.eventClick = new _fsevent.Click();
                self.eventClick.attach(self.clearBtnEl, function() {
                    if (_selector.hasClass(self.clearBtnEl, 'ui-disabled')) {
                        return false;
                    }
                    self.el.value = '';
                    keyupHandler.call(self);
                }, self);
            }
        },
        disabled: function(cmp, isDisabled) {
            if (isDisabled) {
                _selector.addClass(this.el, 'ui-disabled');
                if (this.clearBtnEl) {
                    _selector.addClass(this.clearBtnEl, 'ui-disabled');
                }
            } else {
                _selector.removeClass(this.el, 'ui-disabled');
                if (this.clearBtnEl) {
                    _selector.removeClass(this.clearBtnEl, 'ui-disabled');
                }
            }
        }
    };

    return _parent.subclass({

        xtype: 'jquerymobile',
        className: 'Fs.engines.JqueryMobile',

        constructor: function() {
            var q = _fs.Selector,
                html = q.get('html'),
                body = q.get('body');
            q.addClass(html, 'ui-mobile');
            q.addClass(body, 'ui-mobile-viewport');
        },

        getTpl: function(name) {
            return (_tpls[name] ||
                console.error('JqueryMobile engine: Template "%s" does not exists.', name));
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());


Fs.App = (function() {

    var _fs = Fs,
        _debug = _fs.debug,
        _storage = _fs.Storage,
        _engines = _fs.engines;

    var _getEngine = function(name, defaultEngine) {
        var engine = _engines[name];
        if (engine) {
            return engine;
        }
        if (name) {
            _debug.error('app', 'Given engine "' + name +
                '" does not exists. Using "' + defaultEngine + '" instead.');
        }
        return _engines[defaultEngine];
    };

    return _fs.subclass({

        defaultEngine: 'Homemade',
        templateLoaded: true,
        domReady: false,

        constructor: function(opts) {
            var self = this;

            self.config = opts || {};
            if (typeof self.config.debug !== 'undefined') {
                _debug.set(self.config.debug);
            }
            if (typeof self.config.defaultRoute === 'string') {
                _fs.History.setDefaultRoute(self.config.defaultRoute);
            }
            if (self.config.require && self.config.require.templates) {
                self.templateLoaded = false;
                _fs.Templates.load(self.config.require.templates, function() {
                    self.templateLoaded = true;

                    if (self.domReady === true &&
                        self.config.onready) {
                        self.config.onready();
                    }
                });
            }
            if (self.config.onready) {
                self.registerListener();
            }
            
            _fs.config.engine = _getEngine(self.config.engine, self.defaultEngine);
            if (self.config.ui) {
                _fs.config.ui = self.config.ui;
            }
        },

        registerListener: function() {

            var self = this;

            function ready() {
                if (!self.domReady) {
                    document.removeEventListener('DOMContentLoaded', ready);
                }

                self.domReady = true;
                if (self.templateLoaded === true) {
                    self.config.onready();
                }
            };

            document.addEventListener('DOMContentLoaded', ready);
        }
    });

})();
