/* Layout page */

App.views.Layout = (function() {

    var _fs = Fs,
        _parent = _fs.views.Page,
        _btnTpl = {
            xtype: 'tabbutton',
            config: {
                id: 'menu-{{name}}-btn',
                //icon: '{{icon}}',
                route: '/{{name}}',
                text: '{{name}}'
            }
        };

    return _parent.subclass({
        constructor: function(opts) {
            // @CONFIG
            var buttons = this.parseConfig(window.appConfig);

            opts = _fs.utils.applyIfAuto(opts || {}, {
                items: {
                    xtype: 'footer',
                    config: {
                        ui: 'f',
                        id: 'page-footer',
                        position: 'fixed'
                    },
                    items: {
                        xtype: 'tabs',
                        config: {
                            id: 'main-menu',
                            mini: false
                        },
                        items: buttons
                    }
                }
            });
            _parent.prototype.constructor.call(this, opts);
        },

        parseConfig: function(conf) {
            if (typeof conf !== 'object' ||
                !conf.sections) {
                return false;
            }

            var btn, tab,
                i = -1,
                tabs = conf.sections,
                length = tabs.length,
                btns = [];

            while (++i < length) {
                tab = tabs[i];
                btn = JSON.parse(JSON.stringify(_btnTpl));

                btn.config.id = btn.config.id.replace('{{name}}', tab.name);
                btn.config.route = btn.config.route.replace('{{name}}', tab.name);
                btn.config.text = btn.config.text.replace('{{name}}', tab.name);

                btns.push(btn);
            }

            console.log('btns: ', btns);

            return btns;
        }
    });
}());