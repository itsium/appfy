(function() {

    //Fs.Templates.setPath('/assets/mobile/templates/');

    var app = new Fs.App({
        xrequire: {
            templates: [
            'moremenu',
            'newsdetail',
            'newsdetaildate',
            'newsdetailmap',
            'newssearchcategorylist',
            'newssearchlist',
            'userfavoriteslist'
            ]
        },
        defaultRoute: '',
        engine: 'JqueryMobile',
        ui: 'c',
        debug: false,
        onready: function() {
            var main = new App.controllers.Main();
        }
    });

}());