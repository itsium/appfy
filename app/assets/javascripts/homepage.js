// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require jquery.fitvids
//= require jquery.scrollUp.min


/*
 * All the plugins init are in this file
 *
 */
var map;
$(document).ready(function() {

  // Scroll to top
  $.scrollUp({
     scrollText: '', // Text for element
  });

  // activate carousels
  $('#mobile-carousel').carousel();

  // init fitvids plugin
  $(".video").fitVids();


  // sliding contact form
  $('.contact-btn').click( function(){
    if($(this).hasClass('closed')) {
      $('.contact-form-inner').toggle();
      $(this).removeClass('closed').addClass('open');
    } else {
      $('.contact-form-inner').toggle();
      $(this).removeClass('open').addClass('closed');
    }
  });

  // ajax contact form
  $('.contact-form').submit(function(){
    $.post('contact-form.php', $(this).serialize(), function(data){
      $('.contact-form').html(data);
      $('.contact-form input, .contact-form textarea').val('');
    });
    return false;
  });

  // ajax subscription
  $('.subsc-form').submit(function(){
    $.post('subscription.php', $(this).serialize(), function(data){

      $('.subsc-form > *').fadeIn();
      $('.subsc-form').html(data);
      $('.subsc-form input').val('');
    });
    return false;
  });

});