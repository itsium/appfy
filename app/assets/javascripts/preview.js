//= require preview/vendors/handlebars.runtime
//= require preview/vendors/fastclick
//= require preview/vendors/all.jquerymobile

//= require preview/src/bootstrap
//= require preview/src/views/welcome
//= require preview/src/views/layout
//= require preview/src/controllers/main
//= require preview/src/app

/* debug mode */
window.onerror = function(message, url, linenumber) {
    var error = [
        'Preview error: ',
        message,
        url,
        linenumber
    ];
    window.parent.console.debug.apply(console, error);
};

window.console = window.parent.console;
window.parent.preview = window;