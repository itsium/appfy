module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def current_page_name
    app_id = params[:id]
    app_id = params[:app_id] if params[:app_id]

    (controller.controller_name == "apps" && app_id != nil) ? 
      "menu-#{controller.controller_name}-#{controller.action_name}-#{app_id}"  : 
      "menu-#{controller.controller_name}-#{controller.action_name}"
  end  

  def current_app
    app_id = params[:id]
    app_id = params[:app_id] if params[:app_id]
    (controller.controller_name == "apps" && app_id != nil) ? 
      "menu-#{controller.controller_name}-#{app_id}"  : 
      "menu-#{controller.controller_name}-#{controller.action_name}"
  end
end
